<style type="text/css">
#user-nav-menu{
  list-style-type: none;
  display: inline;
}
#user-nav-menu li{
 display: inline;
 padding-left: 15px;
 padding-right: 15px;
}

.sidebar-widgets-wrap .recent-item .recent-title a {
  color: #73a02e;
  font-weight: normal;
}
.sidebar-widgets-wrap .recent-item .recent-title a:hover {
  color: #333;
}
</style>
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="sidebar-widgets-wrap">
      <div class="sidebar-widget mb-40">
        <!-- <h5 class="mb-20">Menu</h5> -->
        <div class="widget-link">
          <ul id="user-nav-menu">
            <li> <a href="User-Home"> Dashboard</a></li>
            <li> <a href="orders"> Orders</a></li>
            <li> <a href="inquiries"> Inquiries</a></li>
            <li> <a href="profile"> Profile</a></li>
            <li> <a href="Change-Password"> Change Password</a></li>
            <li> <a href="logout"> Logout</a></li>
          </ul>
          <hr />
        </div>
      </div>
    </div>
  </div>
</div>
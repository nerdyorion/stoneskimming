<div class="col-lg-9" style="margin-top: 30px;">

  <!-- <h3>Register</h3> -->
  <?php if($error_code == 0 && !empty($error)): ?>
    <div class="alert alert-success alert-dismissable" style="background-color: #ffffff">
      <!-- <div class="alert alert-success alert-dismissable fade in"> -->
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $error; ?>
      </div>
    <?php elseif($error_code == 1 && !empty($error)): ?>
      <div class="alert alert-danger alert-dismissable" style="background-color: #ffffff">
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error!</strong> <?php echo $error; ?>
      </div>
    <?php else: ?>
    <?php endif; ?>


    <?php $this->load->view('side-nav-menu'); echo "\n"; ?>

    <h6>Welcome <?php echo isset($_SESSION['user_full_name']) ? explode(" ", $_SESSION['user_full_name'])[0] : "User"?>,</h6>
<!--=================================
  side-nav-menu -->

  <h4 class="title"><u>Order Complete</u></h4>

  <div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xx-12 xs-mb-30">
    <div class="product-info mt-20">
      <!-- <h3>Orders</h3> -->
      <img src="<?php echo base_url(); ?>assets/images/order-complete.png" class="img-responsive" style="max-height: 150px; float: left;" class="align-left" /><br /><br /><br />
      <h4 class="text-left align-left"> Your order has been placed successfully. </h4>
      <p>We will contact you on delivery shortly.</p>
      <p><br/><a href='orders'>View all &raquo;</a></p>
    </div>
  </div>
</div>
</div>
<!-- /.col-lg-9 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php $this->load->view('footer'); echo "\n"; ?>

<script type="text/javascript">
  function validate()
  {
    var $btn = $('button[type="submit"]').button('loading');
    $(':input[type="submit"]').prop('disabled', true);
    $('button[type="submit"]').prop('disabled', true);
    return true;
  }
</script>

</body>

</html>
<div class="col-lg-9" style="margin-top: 30px;">

  <!-- <h3>Register</h3> -->
  <?php if($error_code == 0 && !empty($error)): ?>
    <div class="alert alert-success alert-dismissable" style="background-color: #ffffff">
      <!-- <div class="alert alert-success alert-dismissable fade in"> -->
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $error; ?>
      </div>
    <?php elseif($error_code == 1 && !empty($error)): ?>
      <div class="alert alert-danger alert-dismissable" style="background-color: #ffffff">
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error!</strong> <?php echo $error; ?>
      </div>
    <?php else: ?>
    <?php endif; ?>


    <?php $this->load->view('side-nav-menu'); echo "\n"; ?>

    <h6>Welcome <?php echo isset($_SESSION['user_full_name']) ? explode(" ", $_SESSION['user_full_name'])[0] : "User"?>,</h6>
<!--=================================
  side-nav-menu -->

  <h4 class="title"><u>Dashboard</u></h4>

  <div class="row">
   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xx-12 xs-mb-30">
    <p>Orders</p>
      <h3><?php echo number_format($orders_total); ?></h3> 
  </div>
   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xx-12 xs-mb-30">
    <p>Inquiries</p>
      <h3><?php echo number_format($inquiries_total); ?></h3> 
  </div>
</div>
</div>
<!-- /.col-lg-9 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php $this->load->view('footer'); echo "\n"; ?>

<script type="text/javascript">
  function validate()
  {

    if($('#first_name').val() != '' && $('#last_name').val() != '' && $('#email').val() != '' && $('#password').val() != '' )
    {
      // $('#cover-spin').show();
    }

    $('.hide-loader').click(function () {
      // $('#cover-spin').hide();
    });

    var $btn = $('button[type="submit"]').button('loading');
    $(':input[type="submit"]').prop('disabled', true);
    $('button[type="submit"]').prop('disabled', true);
    return true;
  }
</script>

</body>

</html>
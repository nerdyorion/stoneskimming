<div id="cover-spin"><a href="javascript:void(0);" class="hide-loader" style="position: absolute; left:46%;top:50%;"> <i class="fa fa-close"></i> Hide Loader</a></div>
<style type="text/css">
#cover-spin {
  position:fixed;
  width:100%;
  left:0;right:0;top:0;bottom:0;
  background-color: rgba(255,255,255,0.7);
  z-index:9999;
  display:none;
}

@-webkit-keyframes spin {
  from {-webkit-transform:rotate(0deg);}
  to {-webkit-transform:rotate(360deg);}
}

@keyframes spin {
  from {transform:rotate(0deg);}
  to {transform:rotate(360deg);}
}

#cover-spin::after {
  content:'';
  display:block;
  position:absolute;
  left:48%;top:40%;
  width:40px;height:40px;
  border-style:solid;
  border-color:black;
  border-top-color:transparent;
  border-width: 4px;
  border-radius:50%;
  -webkit-animation: spin .8s linear infinite;
  animation: spin .8s linear infinite;
}
</style>

<div class="col-lg-12">

  <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <div class="carousel-item active">
        <img class="d-block img-fluid" src="assets/images/banner1.jpg" alt="First slide" style="max-height: 400px; width: 100%;">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <h3>Register</h3>
  <?php if($error_code == 0 && !empty($error)): ?>
    <div class="alert alert-success alert-dismissable" style="background-color: #ffffff">
      <!-- <div class="alert alert-success alert-dismissable fade in"> -->
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $error; ?>
      </div>
    <?php elseif($error_code == 1 && !empty($error)): ?>
      <div class="alert alert-danger alert-dismissable" style="background-color: #ffffff">
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error!</strong> <?php echo $error; ?>
      </div>
    <?php else: ?>
    <?php endif; ?>


    <div class="row">


      <div class="col-md-6 col-md-offset-3">
        <div class="pb-50 clearfix">

          <?php echo form_open('/register/secure', 'class="form-horizontal", onsubmit="return validate();"'); ?>
          <div class="section-field mb-20">
            <label class="mb-10" for="first_name">First name* </label>
            <input id="first_name" name="first_name" class="form-control" type="text" placeholder="e.g john" required="required" value="<?php echo $this->session->flashdata('first_name'); ?>" />
          </div>
          <div class="section-field mb-20">
            <label class="mb-10" for="last_name">Last name* </label>
            <input id="last_name" name="last_name" class="form-control" type="text" placeholder="e.g doe" required="required" value="<?php echo $this->session->flashdata('last_name'); ?>" />
          </div>
          <div class="section-field mb-20">
            <label class="mb-10" for="email">Email* </label>
            <input id="email" name="email" class="form-control" type="email" placeholder="e.g johndoe@yahoo.com" required="required" value="<?php echo $this->session->flashdata('email'); ?>" />
          </div>
          <div class="section-field mb-20">
           <label class="mb-10" for="Password">Password* </label>
           <input id="Password" name="password" class="form-control" type="password" placeholder="***********" required="required" />
         </div>

         <div class="section-field mb-30">
           <label class="mb-10" for="confirm_password">Confirm Password* </label>
           <input id="confirm_password" name="confirm_password" class="form-control" type="password" placeholder="***********" required="required" />
         </div>
         <div class="section-field mb-20">
          <label class="mb-10">Country </label>
          <div class="box">
            <select class="form-control" id="country_id" name="country_id">
              <option value="0" selected="selected">-- Select --</option>
              <?php foreach ($countries as $row): ?>
                <option value="<?php echo $row['id']; ?>" <?php echo $row['id'] == $this->session->flashdata('country_id') ? 'selected="selected"' : ''; ?>><?php echo $row['name']; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
        </div>
        <button class="btn btn-primary" type="submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span>Signup</span> <i class='fa fa-check'></i>">
          <span>Signup</span>
          <i class="fa fa-check"></i>
        </button> 
        <p class="mt-20 mb-0">Already have an account? <a href="login"> Login</a></p>
      </form>

<!-- 
        <?php echo form_open('/register/secure', 'class="form-horizontal", onsubmit="return validate();"'); ?>
        <input type="hidden" name="return" value="<?php echo isset($_GET['return']) ? trim($_GET['return']) : ''; ?>" />
        <div class="section-field mb-20">
         <label class="mb-10" for="email">Email* </label>
         <input id="email" class="web form-control" type="email" placeholder="e.g johndoe@yahoo.com" name="email" />
       </div>
       <div class="section-field mb-20">
         <label class="mb-10" for="Password">Password* </label>
         <input id="Password" class="Password form-control" type="password" placeholder="***********" name="password" />
       </div>
       <div class="section-field">
        <div class="remember-checkbox mb-30">
                   <a href="Forgot-Password" class="pull-right">Forgot Password?</a>
                 </div>
               </div>
               <button class="btn btn-primary" type="submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span>Log in</span> <i class='fa fa-check'></i>">
                <span>Log in</span>
                <i class="fa fa-check"></i>
              </button> 
              <p class="mt-20 mb-0">Don't have an account? <a href="register"> Create one here</a></p>
            </form>



          -->
        </div>
        <hr />
      </div>
    </div>

  </div>
  <!-- /.col-lg-9 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php $this->load->view('footer'); echo "\n"; ?>

<script type="text/javascript">
  function validate()
  {

    if($('#first_name').val() != '' && $('#last_name').val() != '' && $('#email').val() != '' && $('#password').val() != '' )
    {
      // $('#cover-spin').show();
    }

    $('.hide-loader').click(function () {
      // $('#cover-spin').hide();
    });

    var $btn = $('button[type="submit"]').button('loading');
    $(':input[type="submit"]').prop('disabled', true);
    $('button[type="submit"]').prop('disabled', true);
    return true;
  }
</script>

</body>

</html>
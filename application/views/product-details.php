
<div class="col-lg-9">
  <div style="width: 90%; margin-top: 25px;">

    <div class="container-fluid text-center bg-grey">
      <!-- <h2>MOBILE CONTENTS</h2> -->
      <!-- <h4>What we have created</h4> -->

      <div class="row text-center">

        <div class="col-sm-12" style="border-bottom: 2px solid #feb25a; margin-bottom: 10px; ">
          <h3 class="text-left"><?php echo $row['name']; ?></h3>
        </div>

        <div class="jumbotron" style="display: inline-block; width: 100%; ">
          <div class="col-sm-6">
            <div class="thumbnail">
              <a href="<?php echo current_url();?>#" class="thumbnail link-thumbnail">
                <img src="assets/images/products/<?php echo $row['image_url']; ?>" alt="<?php echo $row['name']; ?>" class="img-responsive" title="Subscribe to <?php echo $row['name']; ?>">
              </a>
            </div>
          </div>

          <div class="col-sm-12 text-left" style="margin-top: 10px;">
            <h4>Description</h4>
            <p style="color: #000000; font-size: 1em;">
              <?php echo $row['description']; ?>
            </p>
            <p style="color: #000000; font-size: 1em;">
              <b>Category</b>: <?php echo $row['category_name']; ?>
            </p>
            <p style="color: #000000; font-size: 1em;">
              <b>Price</b>: &pound;<?php echo $row['price']; ?>
            </p>
            <p style="color: #000000; font-size: 1em;">
              <b>Man. Date</b>: <?php echo $row['manufactured_date']; ?>
            </p>
            <p style="color: #000000; font-size: 1em;">
              <b>Expiry Date</b>: <?php echo $row['expiry_date']; ?>
            </p>
          </div>

        </div>


      </div>

    </div>
  </div>


</div>
<!-- /.col-lg-9 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php $this->load->view('footer'); echo "\n"; ?>

</body>

</html>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Order Details</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "admin123/"; ?>orders">Orders</a></li>
            <li class="active">Details</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12">
          <div class="white-box">
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <tbody>
                  <tr>
                    <th>Customer</th>
                    <td><?php echo $row['full_name']; ?></td>
                  </tr>
                  <tr>
                    <th>Product(s)</th>
                    <td>
                      <?php $products = explode('###', $row['items']); ?>
                      <ul style="list-style-type: none;">
                        <?php foreach($products as $item_array): ?>
                          <?php $item = explode('@@@', $item_array); //var_dump($item); die; ?>
                                    <!-- 
                                      0 - product_id
                                      1 - quantity
                                      2 - name
                                      3 - price
                                      4- image_url
                                    -->
                                    <li style="border-bottom: 1px solid #f9f9f9;">
                                      <!-- <a href="/products/<?php echo $item[0]; ?>"><?php echo $item[1]; ?> (&pound;<?php echo $item[2]; ?>)</a> -->
                                      <p style="clear: both;"><a class="thumbnail pull-left" href="admin123/products/view/<?php echo $item[0]; ?>"> <img class="media-object" src="assets/images/products/<?php echo $item[4]; ?>" style="width: 30px; height: 30px; margin-right: 5px;"> </a>&nbsp;
                                        <b><a href="admin123/products/view/<?php echo $item[0]; ?>"><?php echo $item[2]; ?></a></b>
                                        <b>&pound;<?php echo $item[3]; ?></b> x<?php echo $item[1]; ?></p>
                                      </li>
                        <?php endforeach; ?>
                      </ul>
                    </td>
                  </tr>
                  <tr>
                    <th>Price</th>
                    <td>&pound;<?php echo number_format($row['total_price']); ?></td>
                  </tr>
                  <tr>
                    <th>Status</th>
                    <td><?php echo $row['status_id'] != '4' ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;' . strtoupper($row['status']) . '&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;' . strtoupper($row['status']) . '&nbsp;</a>'; ?></td>
                  </tr>
                  <tr>
                    <th>Payment Type</th>
                    <td><?php echo $row['payment_type']; ?></td>
                  </tr>
                  <tr>
                    <th>Delivery Address</th>
                    <td>QATAR: <?php echo $row['delivery_address']; ?></td>
                  </tr>
                  <tr>
                    <th>Date</th>
                    <td><?php echo $row['date_created'] == "0000-00-00 00:00:00" || is_null($row['date_created']) ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <p class="text-center">
              <button type="button" class="btn btn-info waves-effect waves-light center" onclick="history.go(-1); return false;">&larr; Back</button>
            </p>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

  <?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
  <script src="assets/js/jasny-bootstrap.js"></script>

</body>
</html>

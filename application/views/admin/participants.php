<!-- Custom CSS -->
<link href="assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet">
<!-- Page Content -->
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row bg-title">
      <div class="col-lg-12">
        <h4 class="page-title"><?php echo $event['name'] . ' ' . $event['year']; ?> Participant(s)</h4>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
          <li><a href="<?php echo base_url() . "admin123/events"; ?>">Events</a></li>
          <li class="active">Participant(s)</li>
        </ol>
      </div>
      <!-- /.col-lg-12 -->
    </div>

    <!-- row -->
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <div class="white-box">
          <!--<h3>Participants</h3>-->
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="<?php echo current_url(); ?>#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
            <li role="presentation"><a href="<?php echo current_url(); ?>#add" aria-controls="add" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add New</span></a></li>
            <li role="presentation"><a href="<?php echo current_url(); ?>#addFromPast" aria-controls="add" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add From Past Event(s)</span></a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="all"> 
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Contact</th>
                        <th>Team</th>
                        <th>Scores</th>
                        <th>Date</th>
                        <th class="text-nowrap">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="4" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                        <?php foreach ($rows as $row): ?>
                          <tr>
                            <td><?php echo $sn++; ?></td>
                            <td><?php echo dashIfEmpty($row['first_name'] . ' ' . $row['last_name']); ?><br />
                              <small>
                                <?php echo dashIfEmpty($row['age']) . 'yrs'; ?> | 
                                <?php echo strtoupper(dashIfEmpty($row['gender'])[0]); ?> | 
                                <?php echo dashIfEmpty($row['category_name']); ?> | 
                                <?php echo dashIfEmpty($row['country_name']); ?> <br />
                                <?php echo (int) $row['paid'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;PAID&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;UNPAID&nbsp;</a>'; ?>
                              </small>
                            </td>
                            <td>
                              <small>
                                <?php if(!empty($row['phone']) && $row['phone'] != '-'): ?>
                                  <i class="fa fa-phone"></i> <?php echo dashIfEmpty($row['phone']); ?><br />
                                <?php endif; ?>
                                <?php if(!empty($row['email']) && $row['email'] != '-'): ?>
                                  <i class="fa fa-envelope"></i> <a href="mailto:<?php echo $row['email']; ?>"><?php echo dashIfEmpty($row['email']); ?></a><br />
                                <?php endif; ?>
                                <?php if(!empty($row['address']) && $row['address'] != '-'): ?>
                                  <i class="fa fa-map-marker"></i> <?php echo dashIfEmpty($row['address']) ?>
                                <?php endif; ?>
                              </small>
                            </td>
                            <td><?php echo dashIfEmpty($row['team_name']); ?></td>
                            <td>
                              <small>Score 1: <?php echo is_null($row['score1']) ? 'NULL' : number_format($row['score1']); ?></small><br />
                              <small>Score 2: <?php echo is_null($row['score2']) ? 'NULL' : number_format($row['score2']); ?></small><br />
                              <small>Score 3: <?php echo is_null($row['score3']) ? 'NULL' : number_format($row['score3']); ?></small><br />
                              <small><b>RESULT: <?php echo is_null($row['score1']) && is_null($row['score2']) && is_null($row['score3']) ? 'NULL' : number_format($row['score1'] + $row['score2'] + $row['score3']); ?></b></small><br />
                            </td>
                            <td><?php echo $row['date_created'] == "0000-00-00 00:00:00" || is_null($row['date_created']) ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
                            <td class="text-nowrap">
                              <a href="admin123/participants/edit/<?php echo $event['id']; ?>/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> 

                              <?php if(!empty($row['team_name'])): ?>
                                <a href="admin123/participants/removeFromTeam/<?php echo $event['id']; ?>/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Remove from Team" onclick="if(confirm('You are attempting to remove participant from this team to an INDIVIDUAL status! Are you sure you want to proceed?')) return true; else return false;"> <span class="text-danger"><i class="fa fa-sign-out text-danger m-r-10"></i></span> </a> 
                              <?php endif; ?>

                              <?php if((int) $row['paid'] == 0): ?>
                                <a href="admin123/participants/markPaid/<?php echo $event['id']; ?>/<?php echo $row['id']; ?>/<?php echo (int) $row['category_id']; ?>/<?php echo (int) $row['team_id']; ?>" data-toggle="tooltip" data-original-title="Mark as Paid" onclick="if(confirm('You are attempting to add bought-ticket entry and update participant status to Paid! Are you sure you want to proceed?')) return true; else return false;"> <span class="text-success"><i class="fa fa-money text-success m-r-10"></i></span> </a> 
                              <?php endif; ?>
                              <?php if((int) $row['paid'] == 1): ?>
                                <a href="admin123/participants/markUnpaid/<?php echo $event['id']; ?>/<?php echo $row['id']; ?>/<?php echo (int) $row['category_id']; ?>/<?php echo (int) $row['team_id']; ?>" data-toggle="tooltip" data-original-title="Mark as Unpaid" onclick="if(confirm('You are attempting to return ticket and update participant status to Unpaid! Are you sure you want to proceed?')) return true; else return false;"> <span class="text-danger"><i class="fa fa-ban text-danger m-r-10"></i></span> </a> 
                              <?php endif; ?>

                              <a href="admin123/participants/delete/<?php echo $event['id']; ?>/<?php echo $row['id']; ?>/<?php echo (int) $row['category_id']; ?>/<?php echo (int) $row['paid']; ?>" data-toggle="tooltip" data-original-title="Delete" onclick="if(confirm('You are attempting to delete every entry of the participant for this event! Are you sure you want to proceed?')) return true; else return false;"> <span class="text-danger"><i class="fa fa-close text-danger m-r-10"></i></span> </a> 
                              <!-- <a href="admin123/participants/view/<?php echo $event['id']; ?>/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-folder-o text-inverse m-r-10"></i> </a>  -->
                            </td>
                          </tr>
                        <?php endforeach; ?>
                      <?php endif; ?>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
            <div role="tabpanel" class="tab-pane" id="add">
              <div class="col-md-12">
                <?php echo form_open('admin123/participants/create/' . $event['id'], 'class="form-horizontal", onsubmit="return validate();"'); ?>
                <div class="form-group">
                  <label for="team_id" class="col-sm-2 control-label">Available Team:</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="team_id" id="team_id">
                      <option value="0" selected="selected">-- Select --</option>
                      <?php foreach ($teams_not_yet_full as $row): ?>
                        <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="first_name" class="col-sm-2 control-label">First name: <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="first_name" maxlength="255" id="first_name" value="" placeholder="" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="last_name" class="col-sm-2 control-label">Last name: <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="last_name" maxlength="255" id="last_name" value="" placeholder="" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="country_id" class="col-sm-2 control-label">Country:</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="country_id" id="country_id">
                      <option value="0" selected="selected">-- Select --</option>
                      <?php foreach ($countries as $row): ?>
                        <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="age" class="col-sm-2 control-label">Age: <span class="text-danger">*</span></label>
                  <div class="col-sm-5">
                    <input type="number" min="1" step="1" class="form-control" name="age" id="age" value="" placeholder="" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="gender" class="col-sm-2 control-label">Gender: <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <select class="form-control" name="gender" id="gender" required="required">
                      <option value="0" selected="selected">-- Select --</option>
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="phone" class="col-sm-2 control-label">Phone: <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <!-- <input type="text" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="phone" maxlength="13" id="phone" value="" placeholder="" required="required" /> -->
                    <input type="text" class="form-control" name="phone" maxlength="50" id="phone" value="" placeholder="" required="required" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email: <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="email" maxlength="255" id="email" value="" placeholder="" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="address" class="col-sm-2 control-label">Address: <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="address" id="address" maxlength="8000" required="required"></textarea>
                  </div>
                </div>
                <!-- Scores -->
                <div class="form-group">
                  <label for="score1" class="col-sm-2 control-label">Score 1:</label>
                  <div class="col-sm-3">
                    <input type="number" class="form-control" name="score1" id="score1" value="" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="score2" class="col-sm-2 control-label">Score 2:</label>
                  <div class="col-sm-3">
                    <input type="number" class="form-control" name="score2" id="score2" value="" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="score3" class="col-sm-2 control-label">Score 3:</label>
                  <div class="col-sm-3">
                    <input type="number" class="form-control" name="score3" id="score3" value="" />
                  </div>
                </div>
                <div class="form-group m-b-0">
                  <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-info waves-effect waves-light">Add New</button>
                  </div>
                </div>
              </form>
            </div>
            <div class="clearfix"></div>
          </div>
          <div role="tabpanel" class="tab-pane" id="addFromPast">
            <div class="col-md-12">
              <?php echo form_open('admin123/participants/reusePastParticipant/' . $event['id'], 'class="form-horizontal", onsubmit="return validateAddFromPast();"'); ?>
              <div class="form-group">
                <label for="former_event_id" class="col-sm-2 control-label">Past Event: <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                  <select class="form-control" name="former_event_id" id="former_event_id">
                    <option value="0" selected="selected">-- Select --</option>
                    <?php foreach ($former_events as $row): ?>
                      <option value="<?php echo $row['id']; ?>"><?php echo $row['name'] . ' (' . $row['year'] . ')'; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="former_participant_id" class="col-sm-2 control-label">Participant: <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                  <select class="form-control" name="former_participant_id" id="former_participant_id">
                    <option value="0" selected="selected">-- Select --</option>
                  </select>
                </div>
              </div>
              <div class="form-group m-b-0">
                <div class="col-sm-offset-3 col-sm-9">
                  <button type="submit" class="btn btn-info waves-effect waves-light">Add</button>
                </div>
              </div>
            </form>
          </div>
          <div class="clearfix"></div>
        </div><div class="col-md-3 pull-right pagination">
          <p><?php echo $links; ?></p>
        </div>
      </div>
    </div>
  </div> 
  <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script src="assets/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">

  $('#date').datepicker({
      //format: "yyyy-mm-dd"
      format: "yyyy-mm-dd",
      startDate: "1900-01-01",
      endDate: "2116-01-01",
      todayBtn: "linked",
      autoclose: true,
      todayHighlight: true
    });

  function validate()
  {
    var gender = document.getElementById("gender").value;
    if(gender == 0 ){
      alert('Please specify gender.');
      return false;
    }
    else {
      var $btn = $('button[type="submit"]').button('loading');
      $(':input[type="submit"]').prop('disabled', true);
      $('button[type="submit"]').prop('disabled', true);
      return true;
    }
  }

  function validateAddFromPast()
  {
    var former_event_id = document.getElementById("former_event_id").value;
    var former_participant_id = document.getElementById("former_participant_id").value;
    if(former_event_id == 0 ){
      alert('Please select event.');
      return false;
    }
    else if(former_participant_id == 0 ){
      alert('Please select participant.');
      return false;
    }
    else
    {
      var $btn = $('button[type="submit"]').button('loading');
      $(':input[type="submit"]').prop('disabled', true);
      $('button[type="submit"]').prop('disabled', true);
      return true;
    }
  }
  $(document).ready(function() {
    $("#former_event_id").change(function() {
      var former_event_id = $(this).val();
      if(former_event_id != "" && former_event_id != null) {
        $.ajax({
          url:"admin123/participants/getParticipantsByEventID/" + former_event_id,
            // data:{c_id:country_id},
            type:'GET',
            success:function(response) {
              var resp = $.trim(response);
              $("#former_participant_id").html(resp);
            }
          });
      } else {
        $("#former_participant_id").html("<option value='0'>-- Select --</option>");
      }
    });

    $('#searchIcon').click(function () {
      $('#searchForm').submit();
    });
  });
</script>

</body>
</html>
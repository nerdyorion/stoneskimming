  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Sponsors</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li class="active">Sponsors</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Sponsors</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
              <li role="presentation"><a href="#add" aria-controls="add" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add New</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <!-- Search Filter -->
                  <div class="row">
                    <?php echo form_open('/admin123/sponsors', 'class="form-inline", method="get", role="form"'); ?>
                    <div class="form-group">
                      <label for="title">Search:</label>
                      <input type="text" class="form-control" name="title" maxlength="2000" id="title" value="<?php echo isset($_GET['title']) ? trim($_GET['title']) : ''; ?>" placeholder="" />
                    </select>
                  </div>
                  <button type="submit" class="btn btn-info waves-effect waves-light"><i class="ti-search"></i> Search</button>
                </form>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Full Name</th>
                      <th>Phone</th>
                      <th>Email</th>
                      <th>Gender</th>
                      <th class="text-nowrap">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(empty($rows)): ?>
                      <tr>
                        <td colspan="6" align="center">No data returned.</td>
                      </tr>
                    <?php else: ?>
                      <?php $sn = 1; foreach ($rows as $row): ?>
                      <tr>
                        <td><?php echo $sn++; ?></td>
                        <td><?php echo $row['first_name'] . ' ' . $row['last_name']; ?>
                          <br /><small>Company: <?php echo dashIfEmpty($row['company_name']); ?></small>
                        </td>
                        <td><?php echo dashIfEmpty($row['phone']); ?></td>
                        <td><a href="mailto:<?php echo $row['email']; ?>"><?php echo dashIfEmpty($row['email']); ?></a></td>
                        <td><?php echo dashIfEmpty($row['gender']); ?></td>
                        <td class="text-nowrap">
                          <a href="admin123/sponsors/edit/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> 
                          <a href="admin123/sponsors/delete/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete"> <span class="text-danger"><i class="fa fa-close text-danger m-r-10" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"></i></span> </a> 
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </tbody>
              </table>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
        <div role="tabpanel" class="tab-pane" id="add">
          <div class="col-md-12">
            <?php echo form_open('admin123/sponsors/create', 'class="form-horizontal", onsubmit="return validate();"'); ?>
            <div class="form-group">
              <label for="first_name" class="col-sm-2 control-label">First Name: <span class="text-danger">*</span></label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="first_name" maxlength="255" id="first_name" value="" placeholder="" required="required">
              </div>
            </div>
            <div class="form-group">
              <label for="last_name" class="col-sm-2 control-label">Last Name: <span class="text-danger">*</span></label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="last_name" maxlength="255" id="last_name" value="" placeholder="" required="required" />
              </div>
            </div>
            <div class="form-group">
              <label for="gender" class="col-sm-2 control-label">Gender: <span class="text-danger">*</span></label>
              <div class="col-sm-10">
                <select class="form-control" name="gender" id="gender" required="required">
                  <option value="0" selected="selected">-- Select --</option>
                  <option value="Male">Male</option>
                  <option value="Female">Female</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="company_name" class="col-sm-2 control-label">Company Name:</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="company_name" maxlength="255" id="company_name" value="" placeholder="" />
              </div>
            </div>
            <div class="form-group">
              <label for="phone" class="col-sm-2 control-label">Phone:</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="phone" maxlength="13" id="phone" value="" placeholder="" />
              </div>
            </div>
            <div class="form-group">
              <label for="email" class="col-sm-2 control-label">Email:</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="email" maxlength="255" id="email" value="" placeholder="" />
              </div>
            </div>
            <div class="form-group m-b-0">
              <div class="col-sm-offset-3 col-sm-9">
                <!-- <button type="submit" class="btn btn-info waves-effect waves-light" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span>Add New</span> <i class='fa fa-check'></i>">Add New</button> -->
                <button type="submit" class="btn btn-info waves-effect waves-light">Add New</button>
              </div>
            </div>
          </form>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
    <div class="col-md-3 pull-right pagination">
      <p><?php echo $links; ?></p>
    </div>
  </div>
</div>
</div> 
<!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
  function validate()
  {
    var gender = document.getElementById("gender").value;
    if(gender == 0 ){
      alert('Please specify gender.');
      return false;
    }
    else {

      var $btn = $('button[type="submit"]').button('loading');
      $(':input[type="submit"]').prop('disabled', true);
      $('button[type="submit"]').prop('disabled', true);
      return true;
    }
  }
</script>

</body>
</html>
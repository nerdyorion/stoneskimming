<!-- Custom CSS -->
<link href="assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Events</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li class="active">Events</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Events</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="<?php echo current_url(); ?>#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
              <li role="presentation"><a href="<?php echo current_url(); ?>#add" aria-controls="add" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add New</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>Year</th>
                          <th>Venue</th>
                          <th>Participant(s)</th>
                          <th>Team(s)</th>
                          <th>Others</th>
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if(empty($rows)): ?>
                          <tr>
                            <td colspan="4" align="center">No data returned.</td>
                          </tr>
                        <?php else: ?>
                          <?php foreach ($rows as $row): ?>
                            <tr>
                              <td><?php echo $sn++; ?></td>
                              <td><?php echo dashIfEmpty($row['name']); ?><br />
                                <small><?php echo dashIfEmpty($row['date']) . ' ' . dashIfEmpty($row['time']); ?></small>
                              </td>
                              <td><?php echo dashIfEmpty($row['year']) ?></td>
                              <td><?php echo dashIfEmpty($row['venue_name']); ?><br />
                                <small>Cap: <?php echo number_format($row['venue_capacity']); ?></small>
                              </td>
                              <td>
                                <a href="admin123/participants/index/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Manage Participants">
                                  <?php echo number_format($row['participants']); ?>
                                </a>
                              </td>
                              <td>
                                <a href="admin123/teams/index/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Manage Teams">
                                  <?php echo number_format($row['teams']); ?>
                                </a>
                              </td>
                              <td>
                                <small>Expenses: <a href="admin123/expenses/index/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Manage Expenses">
                                  <?php echo number_format($row['expenses']); ?>
                                </a></small><br />
                                <small>Sponsors: <a href="admin123/Event-Sponsors/index/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Manage Event Sponsors">
                                  <?php echo number_format($row['event_sponsors']); ?>
                                </a></small><br />
                              </td>
                              <td class="text-nowrap">
                                <a href="admin123/events/edit/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> 
                                <a href="admin123/events/delete/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete" onclick="if(confirm('Deleting an event would delete all records associated with it (e.g teams, participants, e.t.c)! Are you sure you want to proceed?')) return true; else return false;"> <span class="text-danger"><i class="fa fa-close text-danger m-r-10"></i></span> </a> 

                                <!-- Manage Participants -->
                                <a href="admin123/participants/index/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Manage Participants"> <i class="fa fa-user text-inverse m-r-10"></i> </a> 
                                
                                <!-- Manage Teams -->
                                <a href="admin123/teams/index/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Manage Teams"> <i class="fa fa-users text-inverse m-r-10"></i> </a> 
                                
                                <!-- View Winners -->
                                <a href="admin123/winners/index/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="View Winners"> <i class="fa fa-trophy text-inverse m-r-10"></i> </a> 
                                
                                <!-- Manage Expenses -->
                                <a href="admin123/expenses/index/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Manage Expenses"> <i class="fa fa-money text-inverse m-r-10"></i> </a> 
                                
                                <!-- Manage Sponsors -->
                                <a href="admin123/Event-Sponsors/index/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Manage Event Sponsors"> <i class="fa fa-handshake-o text-inverse m-r-10"></i> </a> 
                              </td>
                            </tr>
                          <?php endforeach; ?>
                        <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane" id="add">
                <div class="col-md-12">
                  <?php echo form_open('admin123/events/create', 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="name" maxlength="255" id="name" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="date" class="col-sm-2 control-label">Date: <span class="text-danger">*</span></label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" data-date-format="yyyy-mm-dd" name="date" maxlength="10" id="date" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="time" class="col-sm-2 control-label">Time:</label>
                    <div class="col-sm-10">
                      <select class="form-control" name="time" id="time">
                        <option value="0" selected="selected">-- Select --</option>
                        <?php foreach ($event_time as $item): ?>
                          <option value="<?php echo $item; ?>"><?php echo $item; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="venue_id" class="col-sm-2 control-label">Venue: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="venue_id" id="venue_id" required="required">
                        <option value="0" selected="selected">-- Select --</option>
                        <?php foreach ($venues as $row): ?>
                          <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Add New</button>
                    </div>
                  </div>
                </form>
              </div>
              <div class="clearfix"></div>
            </div>
          </div><div class="col-md-3 pull-right pagination">
            <p><?php echo $links; ?></p>
          </div>
        </div>
      </div>
    </div> 
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script src="assets/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">

  $('#date').datepicker({
    //format: "yyyy-mm-dd"
    format: "yyyy-mm-dd",
    startDate: "1900-01-01",
    endDate: "2116-01-01",
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true
  });

  function validate()
  {
    var $btn = $('button[type="submit"]').button('loading');
    $(':input[type="submit"]').prop('disabled', true);
    $('button[type="submit"]').prop('disabled', true);
    return true;
  }
</script>

</body>
</html>
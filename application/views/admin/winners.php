<!-- Page Content -->
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row bg-title">
      <div class="col-lg-12">
        <h4 class="page-title"><?php echo $event['name'] . ' ' . $event['year']; ?> Winner(s)</h4>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
          <li><a href="<?php echo base_url() . "admin123/events"; ?>">Events</a></li>
          <li class="active">Winner(s)</li>
        </ol>
      </div>
      <!-- /.col-lg-12 -->
    </div>

    <!-- row -->
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <div class="white-box">
          <!--<h3>Participants</h3>-->
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="<?php echo current_url(); ?>#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="all"> 
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Contact</th>
                        <th>Team</th>
                        <th>Scores</th>
                        <th>Date Added</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="4" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                        <?php foreach ($rows as $row): ?>
                          <tr>
                            <td><?php echo $sn++; ?></td>
                            <td><?php echo dashIfEmpty($row['first_name'] . ' ' . $row['last_name']); ?><br />
                              <small>
                                <?php echo dashIfEmpty($row['age']) . 'yrs'; ?> | 
                                <?php echo strtoupper(dashIfEmpty($row['gender'])[0]); ?> | 
                                <?php echo dashIfEmpty($row['category_name']); ?> | 
                                <?php echo dashIfEmpty($row['country_name']); ?> <br />
                                <?php echo (int) $row['paid'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;PAID&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;UNPAID&nbsp;</a>'; ?>
                              </small>
                            </td>
                            <td>
                              <small>
                                <?php if(!empty($row['phone']) && $row['phone'] != '-'): ?>
                                  <i class="fa fa-phone"></i> <?php echo dashIfEmpty($row['phone']); ?><br />
                                <?php endif; ?>
                                <?php if(!empty($row['email']) && $row['email'] != '-'): ?>
                                  <i class="fa fa-envelope"></i> <a href="mailto:<?php echo $row['email']; ?>"><?php echo dashIfEmpty($row['email']); ?></a><br />
                                <?php endif; ?>
                                <?php if(!empty($row['address']) && $row['address'] != '-'): ?>
                                  <i class="fa fa-map-marker"></i> <?php echo dashIfEmpty($row['address']) ?>
                                <?php endif; ?>
                              </small>
                            </td>
                            <td><?php echo dashIfEmpty($row['team_name']); ?></td>
                            <td>
                              <small>Score 1: <?php echo is_null($row['score1']) ? 'NULL' : number_format($row['score1']); ?></small><br />
                              <small>Score 2: <?php echo is_null($row['score2']) ? 'NULL' : number_format($row['score2']); ?></small><br />
                              <small>Score 3: <?php echo is_null($row['score3']) ? 'NULL' : number_format($row['score3']); ?></small><br />
                              <small><b>RESULT: <?php echo is_null($row['score1']) && is_null($row['score2']) && is_null($row['score3']) ? 'NULL' : number_format($row['score1'] + $row['score2'] + $row['score3']); ?></b></small><br />
                            </td>
                            <td><?php echo $row['date_created'] == "0000-00-00 00:00:00" || is_null($row['date_created']) ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
                          </tr>
                        <?php endforeach; ?>
                      <?php endif; ?>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
          <div class="col-md-3 pull-right pagination">
          <p><?php echo $links; ?></p>
        </div>
      </div>
    </div>
  </div> 
  <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">

  function validate()
  {
    var gender = document.getElementById("gender").value;
    if(gender == 0 ){
      alert('Please specify gender.');
      return false;
    }
    else {
      var $btn = $('button[type="submit"]').button('loading');
      $(':input[type="submit"]').prop('disabled', true);
      $('button[type="submit"]').prop('disabled', true);
      return true;
    }
  }
</script>

</body>
</html>
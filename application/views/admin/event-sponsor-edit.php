  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Update Event Sponsor</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "admin123/"; ?>events">Events</a></li>
            <li><a href="<?php echo base_url() . "admin123/"; ?>Event-Sponsors/index/<?php echo $event['id']; ?>">Event Sponsors</a></li>
            <li class="active">Edit</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Users</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#update" aria-controls="update" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-pencil"></i></span><span class="hidden-xs"> Update</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="update">
                <div class="col-md-12">
                  <?php echo form_open_multipart('admin123/Event-Sponsors/edit/' . $event['id'] . '/' . $row['sponsor_id'], 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <input type="hidden" name="former_sponsor_id" value="<?php echo $row['sponsor_id']; ?>">
              <div class="form-group">
                <label for="sponsor_id" class="col-sm-2 control-label">Sponsor: <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                  <select class="form-control" name="sponsor_id" id="sponsor_id" disabled="disabled">
                    <option value="" selected="selected">-- Select --</option>
                    <?php foreach ($sponsors as $sponsor): ?>
                      <option value="<?php echo $sponsor['id']; ?>" <?php echo $row['sponsor_id'] == $sponsor['id'] ? 'selected="selected"' : ''; ?>><?php echo $sponsor['first_name'] . ' ' . $sponsor['last_name'] . ' (' . $sponsor['company_name'] . ')'; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
                <div class="form-group">
                  <label for="amount" class="col-sm-2 control-label">Amount (&pound;): <span class="text-danger">*</span></label>
                  <div class="col-sm-5">
                    <input type="number" class="form-control" name="amount" min="0" step="1" id="amount" value="<?php echo $row['amount']; ?>" placeholder="" required="required" />
                  </div>
                </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Update</button>
                      <button type="button" class="btn btn-info waves-effect waves-light center" onclick="history.go(-1); return false;">&larr; Back</button>
                    </div>
                  </div>
                </form>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </div> 
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n"; ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
  function validate()
  {
    var $btn = $('button[type="submit"]').button('loading');
    $(':input[type="submit"]').prop('disabled', true);
    $('button[type="submit"]').prop('disabled', true);
    return true;
  }
</script>

</body>
</html>
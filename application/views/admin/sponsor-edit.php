  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Update Sponsor</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "admin123/"; ?>sponsors">Sponsors</a></li>
            <li class="active">Edit</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Sponsors</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#update" aria-controls="update" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-pencil"></i></span><span class="hidden-xs"> Update</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="update">
                <div class="col-md-12">
                  <?php echo form_open('admin123/sponsors/edit/' . $row["id"], 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="form-group">
                    <label for="first_name" class="col-sm-2 control-label">First Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="first_name" maxlength="255" id="first_name" value="<?php echo $row['first_name']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="last_name" class="col-sm-2 control-label">Last Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="last_name" maxlength="255" id="last_name" value="<?php echo $row['last_name']; ?>" placeholder="" required="required" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="gender" class="col-sm-2 control-label">Gender: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="gender" id="gender" required="required">
                        <option value="0" selected="selected">-- Select --</option>
                        <option value="Male" <?php echo strtolower($row['gender']) == "male" ? 'selected="selected"' : ''; ?>>Male</option>
                        <option value="Female" <?php echo strtolower($row['gender']) == "female" ? 'selected="selected"' : ''; ?>>Female</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="company_name" class="col-sm-2 control-label">Company Name:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="company_name" maxlength="255" id="company_name" value="<?php echo $row['company_name']; ?>" placeholder="" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Email:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="email" maxlength="255" id="email" value="<?php echo $row['email']; ?>" placeholder="" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="phone" class="col-sm-2 control-label">Phone:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="phone" maxlength="13" id="phone" value="<?php echo $row['phone']; ?>" placeholder="" />
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Update</button>
                    </div>
                  </div>
                </form>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </div> 
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
  function validate()
  {
    var gender = document.getElementById("gender").value;
    if(gender == 0 ){
      alert('Please specify gender.');
      return false;
    }
    else {
      var $btn = $('button[type="submit"]').button('loading');
      $(':input[type="submit"]').prop('disabled', true);
      $('button[type="submit"]').prop('disabled', true);
      return true;
    }
  }
</script>

</body>
</html>
  <div class="navbar-default sidebar nicescroll" role="navigation">
    <div class="sidebar-nav navbar-collapse ">
      <ul class="nav" id="side-menu">
        <li class="nav-small-cap">Menu</li>
        <li class=""> <a href="<?php echo base_url() . "admin123/"; ?>" class="waves-effect"><i class="fa fa-dashboard"></i> Dashboard</a> </li>
          <li> <a href="#" class="waves-effect"><i class="ti-pie-chart fa-fw"></i> Setup<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
              <li> <a href="admin123/countries" class="waves-effect"><i class="ti-user fa fa-globe" aria-hidden="true"></i> Countries</a> </li>
              <li> <a href="admin123/categories" class="waves-effect"><i class="ti-cubes fa fa-cubes" aria-hidden="true"></i> Categories</a> </li>
              <li> <a href="admin123/tickets" class="waves-effect"><i class="ti-ticket fa fa-ticket" aria-hidden="true"></i> Tickets</a> </li>
              <li> <a href="admin123/venues" class="waves-effect"><i class="ti-map-marker fa fa-map-marker" aria-hidden="true"></i> Venues</a> </li>

              <!-- <li> <a href="admin123/Payment-Types" class="waves-effect"><i class="ti-money fa fa-money" aria-hidden="true"></i> Payment Types</a> </li>
              <li> <a href="admin123/faqs" class="waves-effect"><i class="ti-question fa fa-question" aria-hidden="true"></i> FAQs</a> </li> -->
            </ul>
          </li>
          <li> <a href="admin123/sponsors" class="waves-effect"><i class="ti-handshake fa fa-handshake-o" aria-hidden="true"></i> Sponsors</a> </li>
          <li> <a href="admin123/events" class="waves-effect"><i class="ti-th fa fa-th" aria-hidden="true"></i> Events</a> </li>
          <!-- <li> <a href="admin123/products" class="waves-effect"><i class="ti-gift fa fa-gift" aria-hidden="true"></i> Products</a> </li>
          <li> <a href="admin123/orders" class="waves-effect"><i class="ti-tag fa fa-tag" aria-hidden="true"></i> Orders</a> </li>
          <li> <a href="admin123/customers" class="waves-effect"><i class="ti-user fa fa-user" aria-hidden="true"></i> Customers</a> </li>
          <li> <a href="admin123/inquiries" class="waves-effect"><i class="ti-comment fa fa-comment" aria-hidden="true"></i> Inquiries</a> </li> -->
          
        <?php if(($_SESSION['user_role_id'] == 1) && (strtoupper($_SESSION['user_role_name']) == "SUPER ADMINISTRATOR")): ?>
          <li> <a href="admin123/users" class="waves-effect"><i class="ti-user fa fa-user"></i> Users (Admin)</a> </li>
        <?php endif; ?>
      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>
<!-- Page Content -->
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row bg-title">
      <div class="col-lg-12">
        <h4 class="page-title"><?php echo $event['name'] . ' ' . $event['year']; ?> Expense(s)</h4>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
          <li><a href="<?php echo base_url() . "admin123/events"; ?>">Events</a></li>
          <li class="active">Expense(s)</li>
        </ol>
      </div>
      <!-- /.col-lg-12 -->
    </div>

    <!-- row -->
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <div class="white-box">
          <!--<h3>Expenses</h3>-->
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="<?php echo current_url(); ?>#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
            <li role="presentation"><a href="<?php echo current_url(); ?>#add" aria-controls="add" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add New</span></a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="all"> 
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th class="text-nowrap">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="4" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                        <?php foreach ($rows as $row): ?>
                          <tr>
                            <td><?php echo $sn++; ?></td>
                            <td><?php echo dashIfEmpty($row['name']); ?></td>
                            <td><?php echo number_format($row['price']); ?></td>
                            <td><?php echo (int) $row['paid'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;PAID&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;UNPAID&nbsp;</a>'; ?></td>
                            <td class="text-nowrap">
                              <a href="admin123/expenses/edit/<?php echo $event['id']; ?>/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> 
                              <a href="admin123/expenses/delete/<?php echo $event['id']; ?>/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"> <span class="text-danger"><i class="fa fa-close text-danger m-r-10"></i></span> </a> 
                            </td>
                          </tr>
                        <?php endforeach; ?>
                      <?php endif; ?>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
            <div role="tabpanel" class="tab-pane" id="add">
              <div class="col-md-12">
                <?php echo form_open('admin123/expenses/create/' . $event['id'], 'class="form-horizontal", onsubmit="return validate();"'); ?>
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Name: <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" maxlength="2000" id="name" value="" placeholder="" required="required" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="price" class="col-sm-2 control-label">Price (&pound;): <span class="text-danger">*</span></label>
                  <div class="col-sm-5">
                    <input type="number" class="form-control" name="price" min="0" step="1" id="price" value="" placeholder="" required="required" />
                  </div>
                </div>
                <div class="form-group">
                  <div class="form-inline row col-sm-offset-2"><!-- col-sm-offset-2  -->
                    <div class="form-group col-sm-10">
                      <div class="checkbox checkbox-primary">
                        <input name="paid" id="paid" type="checkbox" value="1" />
                        <label for="paid"> Paid? </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group m-b-0">
                  <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-info waves-effect waves-light">Add New</button>
                  </div>
                </div>
              </form>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-3 pull-right pagination">
          <p><?php echo $links; ?></p>
        </div>
      </div>
    </div>
  </div> 
  <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script src="assets/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">

  function validate()
  {
    var $btn = $('button[type="submit"]').button('loading');
    $(':input[type="submit"]').prop('disabled', true);
    $('button[type="submit"]').prop('disabled', true);
    return true;
  }
</script>

</body>
</html>
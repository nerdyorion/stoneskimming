  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Update Participant</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "admin123/"; ?>events">Events</a></li>
            <li><a href="<?php echo base_url() . "admin123/"; ?>participants/index/<?php echo $event['id']; ?>">Participants</a></li>
            <li class="active">Edit</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Users</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#update" aria-controls="update" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-pencil"></i></span><span class="hidden-xs"> Update</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="update">
                <div class="col-md-12">
                <?php echo form_open_multipart('admin123/participants/edit/' . $event['id'] . '/' . $row['id'], 'class="form-horizontal", onsubmit="return validate();"'); ?>
                <input type="hidden" name="former_team_id" value="<?php echo (int) $row['team_id'];  ?>" />
                  <div class="form-group">
                    <label for="team_id" class="col-sm-2 control-label">Available Team:</label>
                    <div class="col-sm-10">
                      <select class="form-control" name="team_id" id="team_id">
                        <option value="0">-- Select --</option>
                        <?php foreach ($teams_not_yet_full as $team): ?>
                          <option value="<?php echo $team['id']; ?>" <?php echo $row['team_id'] == $team['id'] ? 'selected="selected"' : ''; ?>><?php echo $team['name']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="first_name" class="col-sm-2 control-label">First name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="first_name" maxlength="255" id="first_name" value="<?php echo $row['first_name']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="last_name" class="col-sm-2 control-label">Last name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="last_name" maxlength="255" id="last_name" value="<?php echo $row['last_name']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="country_id" class="col-sm-2 control-label">Country:</label>
                    <div class="col-sm-10">
                      <select class="form-control" name="country_id" id="country_id">
                        <option value="0" selected="selected">-- Select --</option>
                        <?php foreach ($countries as $country): ?>
                          <option value="<?php echo $country['id']; ?>" <?php echo $row['country_id'] == $country['id'] ? 'selected="selected"' : ''; ?>><?php echo $country['name']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="age" class="col-sm-2 control-label">Age: <span class="text-danger">*</span></label>
                    <div class="col-sm-5">
                      <input type="number" min="1" step="1" class="form-control" name="age" id="age" value="<?php echo $row['age']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="gender" class="col-sm-2 control-label">Gender: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="gender" id="gender" required="required">
                        <option value="0">-- Select --</option>
                        <option value="Male" <?php echo strtolower($row['gender']) == "male" ? 'selected="selected"' : ''; ?>>Male</option>
                        <option value="Female" <?php echo strtolower($row['gender']) == "female" ? 'selected="selected"' : ''; ?>>Female</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="phone" class="col-sm-2 control-label">Phone: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <!-- <input type="text" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="phone" maxlength="13" id="phone" value="" placeholder="" required="required" /> -->
                      <input type="text" class="form-control" name="phone" maxlength="50" id="phone" value="<?php echo $row['phone']; ?>" placeholder="" required="required" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Email: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="email" maxlength="255" id="email" value="<?php echo $row['email']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">Address: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="address" id="address" maxlength="8000" required="required"><?php echo $row['address']; ?></textarea>
                    </div>
                  </div>
                <!-- Scores -->
                <div class="form-group">
                  <label for="score1" class="col-sm-2 control-label">Score 1:</label>
                  <div class="col-sm-3">
                    <input type="number" class="form-control" name="score1" id="score1" value="<?php echo $row['score1']; ?>" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="score2" class="col-sm-2 control-label">Score 2:</label>
                  <div class="col-sm-3">
                    <input type="number" class="form-control" name="score2" id="score2" value="<?php echo $row['score2']; ?>" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="score3" class="col-sm-2 control-label">Score 3:</label>
                  <div class="col-sm-3">
                    <input type="number" class="form-control" name="score3" id="score3" value="<?php echo $row['score3']; ?>" />
                  </div>
                </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Update</button>
                      <button type="button" class="btn btn-info waves-effect waves-light center" onclick="history.go(-1); return false;">&larr; Back</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n"; ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
    function validate()
    {
      var $btn = $('button[type="submit"]').button('loading');
      $(':input[type="submit"]').prop('disabled', true);
      $('button[type="submit"]').prop('disabled', true);
      return true;
    }
</script>

</body>
</html>
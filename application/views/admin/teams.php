<!-- Custom CSS -->
<link href="assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet">
<!-- Page Content -->
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row bg-title">
      <div class="col-lg-12">
        <h4 class="page-title"><?php echo $event['name'] . ' ' . $event['year']; ?> Team(s)</h4>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
          <li><a href="<?php echo base_url() . "admin123/events"; ?>">Events</a></li>
          <li class="active">Team(s)</li>
        </ol>
      </div>
      <!-- /.col-lg-12 -->
    </div>

    <!-- row -->
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <div class="white-box">
          <!--<h3>Teams</h3>-->
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="<?php echo current_url(); ?>#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
            <li role="presentation"><a href="<?php echo current_url(); ?>#add" aria-controls="add" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add New</span></a></li>
            <li role="presentation"><a href="<?php echo current_url(); ?>#addFromPast" aria-controls="add" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add From Past Event(s)</span></a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="all"> 
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Participants</th>
                        <th>Date</th>
                        <th class="text-nowrap">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="4" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                        <?php foreach ($rows as $row): ?>
                          <tr>
                            <td><?php echo $sn++; ?></td>
                            <td><?php echo dashIfEmpty($row['name']); ?></td>
                            <td><?php echo dashIfEmpty($row['description']); ?></td>
                            <td><?php echo number_format($row['participants']); ?></td>
                            <td><?php echo $row['date_created'] == "0000-00-00 00:00:00" || is_null($row['date_created']) ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
                            <td class="text-nowrap">
                              <a href="admin123/teams/edit/<?php echo $event['id']; ?>/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> 
                              <a href="admin123/teams/delete/<?php echo $event['id']; ?>/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete" onclick="if(confirm('You are attempting to remove all participants in this team to an INDIVIDUAL status and delete this team from this event! Are you sure you want to proceed?')) return true; else return false;"> <span class="text-danger"><i class="fa fa-close text-danger m-r-10"></i></span> </a> 
                            </td>
                          </tr>
                        <?php endforeach; ?>
                      <?php endif; ?>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
            <div role="tabpanel" class="tab-pane" id="add">
              <div class="col-md-12">
                <?php echo form_open('admin123/teams/create/' . $event['id'], 'class="form-horizontal", onsubmit="return validate();"'); ?>
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Team Name: <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" maxlength="2000" id="name" value="" placeholder="" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="description" class="col-sm-2 control-label">Description:</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="description" id="description" maxlength="8000"></textarea>
                  </div>
                </div>
                <div class="form-group m-b-0">
                  <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-info waves-effect waves-light">Add New</button>
                  </div>
                </div>
              </form>
            </div>
            <div class="clearfix"></div>
          </div>
          <div role="tabpanel" class="tab-pane" id="addFromPast">
            <div class="col-md-12">
              <?php echo form_open('admin123/teams/reusePastTeam/' . $event['id'], 'class="form-horizontal", onsubmit="return validateAddFromPast();"'); ?>
              <div class="form-group">
                <label for="former_event_id" class="col-sm-2 control-label">Past Event: <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                  <select class="form-control" name="former_event_id" id="former_event_id">
                    <option value="0" selected="selected">-- Select --</option>
                    <?php foreach ($former_events as $row): ?>
                      <option value="<?php echo $row['id']; ?>"><?php echo $row['name'] . ' (' . $row['year'] . ')'; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="former_team_id" class="col-sm-2 control-label">Team: <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                  <select class="form-control" name="former_team_id" id="former_team_id">
                    <option value="0" selected="selected">-- Select --</option>
                  </select>
                </div>
              </div>
              <div class="form-group m-b-0">
                <div class="col-sm-offset-3 col-sm-9">
                  <button type="submit" class="btn btn-info waves-effect waves-light">Add</button>
                </div>
              </div>
            </form>
          </div>
          <div class="clearfix"></div>
        </div><div class="col-md-3 pull-right pagination">
          <p><?php echo $links; ?></p>
        </div>
      </div>
    </div>
  </div> 
  <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script src="assets/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">

  $('#date').datepicker({
    //format: "yyyy-mm-dd"
    format: "yyyy-mm-dd",
    startDate: "1900-01-01",
    endDate: "2116-01-01",
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true
  });

  function validate()
  {
    var $btn = $('button[type="submit"]').button('loading');
    $(':input[type="submit"]').prop('disabled', true);
    $('button[type="submit"]').prop('disabled', true);
    return true;
  }

  function validateAddFromPast()
  {
    var former_event_id = document.getElementById("former_event_id").value;
    var former_team_id = document.getElementById("former_team_id").value;
    if(former_event_id == 0 ){
      alert('Please select event.');
      return false;
    }
    else if(former_team_id == 0 ){
      alert('Please select team.');
      return false;
    }
    else
    {
      var $btn = $('button[type="submit"]').button('loading');
      $(':input[type="submit"]').prop('disabled', true);
      $('button[type="submit"]').prop('disabled', true);
      return true;
    }
  }
  $(document).ready(function() {
    $("#former_event_id").change(function() {
      var former_event_id = $(this).val();
      if(former_event_id != "" && former_event_id != null) {
        $.ajax({
          url:"admin123/teams/getTeamsByEventID/" + former_event_id,
            // data:{c_id:country_id},
            type:'GET',
            success:function(response) {
              var resp = $.trim(response);
              $("#former_team_id").html(resp);
            }
          });
      } else {
        $("#former_team_id").html("<option value='0'>-- Select --</option>");
      }
    });

    $('#searchIcon').click(function () {
      $('#searchForm').submit();
    });
  });
</script>

</body>
</html>
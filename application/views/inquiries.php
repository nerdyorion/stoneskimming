<div class="col-lg-9" style="margin-top: 30px;">

  <!-- <h3>Register</h3> -->

    <?php $this->load->view('side-nav-menu'); echo "\n"; ?>

    <!-- <h6>Welcome <?php // echo isset($_SESSION['user_full_name']) ? explode(" ", $_SESSION['user_full_name'])[0] : "User"?>,</h6> -->
<!--=================================
  side-nav-menu -->

  <h4 class="title"><u>Inquiries</u></h4>

  <div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xx-12 xs-mb-30">

      <a class="button small mt-0" href="inquiries/add"><i class="fa fa-plus"></i> Make Inquiry</a>


  <?php if($error_code == 0 && !empty($error)): ?>
    <div class="alert alert-success alert-dismissable" style="background-color: #ffffff">
      <!-- <div class="alert alert-success alert-dismissable fade in"> -->
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $error; ?>
      </div>
    <?php elseif($error_code == 1 && !empty($error)): ?>
      <div class="alert alert-danger alert-dismissable" style="background-color: #ffffff">
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error!</strong> <?php echo $error; ?>
      </div>
    <?php else: ?>
    <?php endif; ?>


    <?php if(empty($rows)): ?>
     <p>You have not made any inquiries yet! <br />
     </p>
   <?php else: ?>
    <div class="table-responsive table-hover">
      <table class="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Question</th>
            <th>Reply</th>
            <th>Date</th>
            <th>View</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($rows as $row): ?>
           <tr>
            <td><?php echo $sn++; ?></td>
            <td class="description"><?php echo ellipsize($row['question'], 100); ?></td>
            <td class="description"><?php echo is_null($row['reply']) || empty($row['reply']) ? '<mark style="background-color: #FF0800">PENDING</mark>' : ellipsize($row['reply'], 100); ?></td>
              <td class="price"><?php echo $row['date_created'] == "0000-00-00 00:00:00" || is_null($row['date_created']) ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?>
              </td>
              <td>
                <a href="inquiries/view/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-folder-open text-inverse m-r-10"></i> </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  <?php endif; ?>
  <div class=" pull-right">

    <nav aria-label="...">
      <ul class="pagination">
        <?php echo $links; ?>
      </ul>
    </nav>
  </div>
</div>
</div>
</div>
<!-- /.col-lg-9 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php $this->load->view('footer'); echo "\n"; ?>

<script type="text/javascript">
  function validate()
  {
    var $btn = $('button[type="submit"]').button('loading');
    $(':input[type="submit"]').prop('disabled', true);
    $('button[type="submit"]').prop('disabled', true);
    return true;
  }
</script>

</body>

</html>
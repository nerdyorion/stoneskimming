

<div class="col-lg-12">

  <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <div class="carousel-item active">
        <img class="d-block img-fluid" src="assets/images/banner1.jpg" alt="First slide" style="max-height: 400px; width: 100%;">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <h3>Login</h3>
    <?php if($error_code == 0 && !empty($error)): ?>
      <div class="alert alert-success alert-dismissable" style="background-color: #ffffff">
      <!-- <div class="alert alert-success alert-dismissable fade in"> -->
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $error; ?>
      </div>
    <?php elseif($error_code == 1 && !empty($error)): ?>
      <div class="alert alert-danger alert-dismissable" style="background-color: #ffffff">
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error!</strong> <?php echo $error; ?>
      </div>
    <?php else: ?>
    <?php endif; ?>


  <div class="row">


      <div class="col-md-6 col-md-offset-3">
      <div class="pb-50 clearfix">
        <?php echo form_open('/login/secure', 'class="form-horizontal", onsubmit="return validate();"'); ?>
        <input type="hidden" name="return" value="<?php echo isset($_GET['return']) ? trim($_GET['return']) : ''; ?>" />
        <div class="section-field mb-20">
         <label class="mb-10" for="email">Email* </label>
         <input id="email" class="web form-control" type="email" placeholder="e.g johndoe@yahoo.com" name="email" />
       </div>
       <div class="section-field mb-20">
         <label class="mb-10" for="Password">Password* </label>
         <input id="Password" class="Password form-control" type="password" placeholder="***********" name="password" />
       </div>
       <div class="section-field">
        <div class="remember-checkbox mb-30">
                   <a href="Forgot-Password" class="pull-right">Forgot Password?</a>
                 </div>
               </div>
               <button class="btn btn-primary" type="submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span>Log in</span> <i class='fa fa-check'></i>">
                <span>Log in</span>
                <i class="fa fa-check"></i>
              </button> 
              <p class="mt-20 mb-0">Don't have an account? <a href="register"> Create one here</a></p>
            </form>
          </div>
          <hr />
        </div>
    </div>

  </div>
  <!-- /.col-lg-9 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php $this->load->view('footer'); echo "\n"; ?>

<script type="text/javascript">
  function validate()
  {
    $(':input[type="submit"]').prop('disabled', true);
    $('button[type="submit"]').prop('disabled', true);
    return true;
  }
</script>

</body>

</html>
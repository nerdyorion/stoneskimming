<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <base href="<?php echo base_url();?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?php echo $page_title; ?> | <?php echo $this->config->item('app_name'); ?> &#8211; Play to win.</title>


  <!-- Open Graph Tags -->
  <meta property="og:type" content="website" />
  <meta property="og:title" content="<?php echo $this->config->item('app_name'); ?>" />
  <meta property="og:description" content="Buy Chocolate Cakes and other variants." />
  <meta property="og:url" content="http://brilloconnetz.com/ruffles/" />
  <meta property="og:site_name" content="<?php echo $this->config->item('app_name'); ?>" />
  <meta property="og:image" content="http://brilloconnetz.com/ruffles/logo.png" />
  <meta property="og:locale" content="en_US" />
  <meta name="twitter:card" content="summary" />


  <!-- For Google -->
  <meta name="description" content="<?php echo $page_desc; ?>" />
  <meta name="keywords" content="stone, skimming" />

  <meta name="author" content="brilloconnetz.com" />
  <!-- <meta name="copyright" content="<?php echo $this->config->base_url(); ?>terms-and-conditions" /> -->
  <meta name="application-name" content="<?php echo $this->config->item('app_name'); ?>" />

  <!-- For Facebook -->
  <meta property="og:title" content="<?php echo $this->config->item('app_name'); ?>" />
  <meta property="og:type" content="article" />
  <meta property="og:image" content="<?php echo $this->config->base_url(); ?>assets/images/logo.png" />
  <meta property="og:url" content="<?php echo $this->config->base_url(); ?>" />
  <meta property="og:description" content="Stone Skimming Competition" />

  <!-- For Twitter -->
  <meta name="twitter:card" content="summary" />
  <meta name="twitter:title" content="<?php echo $this->config->item('app_name'); ?>" />
  <meta name="twitter:description" content="Stone Skimming Competition" />
  <meta name="twitter:image" content="<?php echo $this->config->base_url(); ?>assets/images/logo.png" />
  <title><?php echo $page_title; ?> | <?php echo $this->config->item('app_name'); ?></title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/images/favicon.jpg" type="image/jpg" />
  <!-- Bootstrap core CSS -->
  <link href="assets/vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Font Awesome -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" /> -->
  <link rel="stylesheet" href="assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css" />



  <!-- jQuery -->
  <script src="assets/vendors/jquery/jquery.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script src="assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>


  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> -->
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

  <!-- Custom styles for this template -->
  <link href="assets/css/shop-homepage.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="assets/css/style.css" rel="stylesheet">

</head>

<body style="margin-top: 0px; padding-top: 0px;">


  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="margin-top: 0px;">
    <div class="container">
      <a class="navbar-brand" href="<?php echo base_url(); ?>">Stone Skimming</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url(); ?>">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <?php if(is_logged_in()): ?>
            <li class="nav-item"><a class="nav-link" href="dashboard"><mark><span class="fa fa-home"></span> Dashboard</mark></a></li>
            <li class="nav-item"><a class="nav-link" href="logout"><span class="fa fa-sign-out"></span> Logout</a></li>
          <?php else: ?>
            <!-- <li class="nav-item"><a class="nav-link" href="register"><span class="fa fa-user"></span> Register</a></li> -->
            <li class="nav-item"><a class="nav-link" href="login"><span class="fa fa-sign-in"></span> Sign In</a></li>
          <?php endif; ?>
      </ul>
    </div>
  </div>
</nav>

<!-- Page Content -->
<div class="container">

  <div class="row">

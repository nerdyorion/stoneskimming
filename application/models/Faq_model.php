<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Faq_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function record_count() {
        return $this->db->count_all("faqs");
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if($id === FALSE)
        {
            $this->db->order_by('question', 'ASC');
            $this->db->select('id, question, answer');
            $this->db->from('faqs'); 
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();
            return $query->result_array();
        }

        $this->db->select('id, question, answer');
        $this->db->from('faqs');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsDropDown($id = FALSE)
    {
        if ($id === FALSE)
        {
            $this->db->order_by('question', 'ASC');
            $this->db->select('id, question, answer');
            $this->db->from('faqs'); 
            $query = $this->db->get();
            
            return $query->result_array();
        }
        $this->db->select('question');
        $this->db->from('faqs');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRandom($limit)
    {
        $limit = (int) $limit;

        $this->db->order_by('id', 'RANDOM');
        $this->db->select('id, question');
        $this->db->from('faqs');
        $this->db->limit($limit);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function add()
    {
        $created_by = (int) $this->session->userdata('user_id');

        $data = array(
            'question' => trim($this->input->post('question')),
            'answer' => trim($this->input->post('answer')),
            'created_by' => $created_by
        );

        $this->db->insert('faqs', $data);
    }

    public function delete($id)
    {
        $this->db->delete('faqs', array('id' => (int) $id));
    }

    public function update($id)
    {

        $data = array(
            'question' => trim($this->input->post('question')),
            'answer' => trim($this->input->post('answer'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('faqs', $data);
    }
}
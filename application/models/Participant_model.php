<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Participant_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function record_count($event_id = FALSE)
    {
        if($event_id !== FALSE)
        {
            $this->db->where('event_id', (int) $event_id);
            $this->db->from('event_participants');
            return $this->db->count_all_results();
        }
        return $this->db->count_all("event_participants");
    }

    public function countCurrentYear()
    {
        $sql = "SELECT COUNT(event_participants.participant_id) AS count FROM `event_participants` LEFT OUTER JOIN `events` ON event_participants.event_id = events.id WHERE events.year = YEAR(CURRENT_DATE)";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['count'];
    }

    public function getRows($limit, $offset, $event_id, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $event_id = (int) $event_id;

        if($id === FALSE)
        {
            $this->db->order_by('event_participants.date_created', 'DESC');
            $this->db->order_by('team_name', 'ASC');
            $this->db->select("participants.id, participants.category_id, participants.first_name, participants.last_name, participants.country_id, participants.age, participants.gender, participants.phone, participants.email, participants.address, event_participants.date_created, scores.score1, scores.score2, scores.score3, scores.result, event_participants.paid, categories.name AS category_name, countries.name AS country_name, (SELECT teams.name FROM team_participants LEFT OUTER JOIN teams ON team_participants.team_id = teams.id WHERE team_participants.event_id = '$event_id' AND team_participants.participant_id = participants.id) AS team_name, (SELECT teams.id FROM team_participants LEFT OUTER JOIN teams ON team_participants.team_id = teams.id WHERE team_participants.event_id = '$event_id' AND team_participants.participant_id = participants.id) AS team_id");
            $this->db->from('event_participants'); 
            $this->db->join('participants', 'event_participants.participant_id = participants.id', 'left');
            $this->db->join('scores', 'event_participants.participant_id = scores.participant_id', 'left');
            $this->db->join('categories', 'participants.category_id = categories.id', 'left');
            $this->db->join('countries', 'participants.country_id = countries.id', 'left');
            $this->db->where('event_participants.event_id', $event_id); 
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();
            return $query->result_array();
        }

        $this->db->select("participants.id, participants.category_id, participants.first_name, participants.last_name, participants.country_id, participants.age, participants.gender, participants.phone, participants.email, participants.address, event_participants.date_created, scores.score1, scores.score2, scores.score3, scores.result, event_participants.paid, categories.name AS category_name, countries.name AS country_name, (SELECT teams.name FROM team_participants LEFT OUTER JOIN teams ON team_participants.team_id = teams.id WHERE team_participants.event_id = '$event_id' AND team_participants.participant_id = participants.id) AS team_name, (SELECT teams.id FROM team_participants LEFT OUTER JOIN teams ON team_participants.team_id = teams.id WHERE team_participants.event_id = '$event_id' AND team_participants.participant_id = participants.id) AS team_id");
        $this->db->from('event_participants');
        $this->db->join('participants', 'event_participants.participant_id = participants.id', 'left');
        $this->db->join('scores', 'event_participants.participant_id = scores.participant_id', 'left');
        $this->db->join('categories', 'participants.category_id = categories.id', 'left');
        $this->db->join('countries', 'participants.country_id = countries.id', 'left');
        $this->db->where('participants.id', $id); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsWinners($event_id)
    {
        $event_id = (int) $event_id;

        
        $this->db->order_by('scores.result', 'DESC');
        $this->db->select("participants.id, participants.category_id, participants.first_name, participants.last_name, participants.country_id, participants.age, participants.gender, participants.phone, participants.email, participants.address, event_participants.date_created, scores.score1, scores.score2, scores.score3, scores.result, event_participants.paid, categories.name AS category_name, countries.name AS country_name, (SELECT teams.name FROM team_participants LEFT OUTER JOIN teams ON team_participants.team_id = teams.id WHERE team_participants.event_id = '$event_id' AND team_participants.participant_id = participants.id) AS team_name, (SELECT teams.id FROM team_participants LEFT OUTER JOIN teams ON team_participants.team_id = teams.id WHERE team_participants.event_id = '$event_id' AND team_participants.participant_id = participants.id) AS team_id");
        $this->db->from('event_participants'); 
        $this->db->join('participants', 'event_participants.participant_id = participants.id', 'left');
        $this->db->join('scores', 'event_participants.participant_id = scores.participant_id', 'left');
        $this->db->join('categories', 'participants.category_id = categories.id', 'left');
        $this->db->join('countries', 'participants.country_id = countries.id', 'left');
        $this->db->where('event_participants.event_id', $event_id); 
        $this->db->where('scores.result IS NOT NULL'); 
        $this->db->limit(3); // get first, second and third

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getRowsDropDown($event_id)
    {
        $this->db->order_by('participants.date_created', 'DESC');
        $this->db->select('participants.id, participants.first_name, participants.last_name');
        $this->db->from('event_participants'); 
        $this->db->join('participants', 'event_participants.participant_id = participants.id', 'left');
        $this->db->where('event_participants.event_id', (int) $event_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTeam($event_id, $participant_id)
    {
        $this->db->select('teams.id, teams.name');
        $this->db->from('event_teams'); 
        $this->db->join('team_participants', 'event_teams.team_id = team_participants.team_id', 'left');
        $this->db->join('teams', 'team_participants.team_id = teams.id', 'left');
        $this->db->where('event_teams.event_id', (int) $event_id);
        $this->db->where('team_participants.participant_id', (int) $participant_id);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function add($event_id)
    {
        $created_by = (int) $this->session->userdata('user_id');
        $event_id = (int) $event_id;
        $team_id = (int) trim($this->input->post('team_id'));

        // add to participants table
        $data = array(
            'category_id' => (int) trim($this->input->post('category_id')),
            'first_name' => trim($this->input->post('first_name')),
            'last_name' => trim($this->input->post('last_name')),
            'country_id' => (int) trim($this->input->post('country_id')),
            'age' => trim($this->input->post('age')),
            'gender' => trim($this->input->post('gender')),
            'phone' => trim($this->input->post('phone')),
            'email' => trim($this->input->post('email')),
            'address' => trim($this->input->post('address')),
            'created_by' => $created_by
        );

        $this->db->insert('participants', $data);

        $participant_id = $this->db->insert_id();

        // add scores to scores table
        $score1 = trim($this->input->post('score1'));
        $score2 = trim($this->input->post('score2'));
        $score3 = trim($this->input->post('score3'));

        // if any of them is not empty, that means one or all of them won't be null
        if(($score1 != '') || ($score2 != '') || ($score3 != ''))
        {
            // set the empty ones to null
            $score1 = $score1 == '' ? NULL : $score1;
            $score2 = $score2 == '' ? NULL : $score2;
            $score3 = $score3 == '' ? NULL : $score3;

            $data = array(
                'event_id' => $event_id,
                'participant_id' => (int) $participant_id,
                'score1' => $score1,
                'score2' => $score2,
                'score3' => $score3,
                'result' => (int) $score1 + (int) $score2 + (int) $score3,
                'created_by' => $created_by
            );
            $this->db->insert('scores', $data);
        }

        // add to event_participants table
        $data = array(
            'event_id' => $event_id,
            'participant_id' => (int) $participant_id,
            'created_by' => $created_by
        );
        $this->db->insert('event_participants', $data);

        // if participant belongs to team, add to team_participants
        if($team_id > 0)
        {
            $data = array(
                'event_id' => $event_id,
                'team_id' => $team_id,
                'participant_id' => (int) $participant_id,
                'created_by' => $created_by
            );
            $this->db->insert('team_participants', $data);
        }
    }

    public function exists($event_id, $participant_id)
    {
        $event_id = (int) $event_id;
        $participant_id = (int) $participant_id;
        
        $this->db->where('event_id', $event_id);
        $this->db->where('participant_id', $participant_id);
        $this->db->from('event_participants');
        $count = $this->db->count_all_results();

        if($count < 1)
        {
            // participant not yet added for event
            return false;
        }
        else
        {
            // participant already added for event
            return true;
        }
    }

    public function reusePastParticipant($event_id)
    {
        $created_by = (int) $this->session->userdata('user_id');
        $event_id = (int) $event_id;

        // add to event_teams table
        $data = array(
            'event_id' => $event_id,
            'participant_id' => (int) trim($this->input->post('former_participant_id')),
            'created_by' => $created_by
        );
        $this->db->insert('event_participants', $data);
    }

    public function delete($event_id, $id, $category_id, $paid)
    {
        $event_id = (int) $event_id;
        $id = (int) $id;
        $category_id = (int) $category_id;
        $paid = (int) $paid;

        // delete from event_participants
        $this->db->delete('event_participants', array('event_id' => $event_id, 'participant_id' => $id));

        // delete from bought_tickets
        $this->db->delete('bought_tickets', array('event_id' => $event_id, 'participant_id' => $id));

        // remove 1 from tickets_sold column in tickets table if participant bought ticket
        if($paid === 1)
        {
            $sql = "UPDATE tickets SET tickets_sold = tickets_sold - 1 WHERE (event_id = '$event_id' AND category_id = '$category_id') AND (tickets_sold != 0)";
            $this->db->query($sql);
        }

        // delete from scores
        $this->db->delete('scores', array('event_id' => $event_id, 'participant_id' => $id));

        $team = $this->getTeam($event_id, $id);
        // if belongs to team, delete from team_participants
        if($team)
        {
            $this->db->delete('team_participants', array('event_id' => $event_id, 'team_id' => $team['id'], 'participant_id' => $id));
        }        

        // if participant is not registered to any other event, you can delete from the main participant table
        $this->db->reset_query();
        $this->db->where('participant_id', $id);
        $this->db->from('event_participants');
        $participant = $this->db->count_all_results();

        if((int) $participant < 1)
        {
            // not registered to any other event, so delete from participant table
            $this->db->delete('participants', array('id' => (int) $id));
        }
        else
        {
            // registered  to another event, so leave it
        }
    }

    public function removeFromTeam($event_id, $id)
    {
        $event_id = (int) $event_id;
        $id = (int) $id;

        // delete from team_participants
        $this->db->delete('team_participants', array('event_id' => $event_id, 'participant_id' => $id));
    }

    public function update($event_id, $id)
    {
        $created_by = (int) $this->session->userdata('user_id');
        $id = (int) $id;
        $event_id = (int) $event_id;

        $team_id = (int) trim($this->input->post('team_id'));
        $former_team_id = (int) trim($this->input->post('former_team_id'));

        $data = array(
            'category_id' => (int) trim($this->input->post('category_id')),
            'first_name' => trim($this->input->post('first_name')),
            'last_name' => trim($this->input->post('last_name')),
            'country_id' => (int) trim($this->input->post('country_id')),
            'age' => trim($this->input->post('age')),
            'gender' => trim($this->input->post('gender')),
            'phone' => trim($this->input->post('phone')),
            'email' => trim($this->input->post('email')),
            'address' => trim($this->input->post('address'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('participants', $data);

        // update scores in scores table
        $score1 = trim($this->input->post('score1'));
        $score2 = trim($this->input->post('score2'));
        $score3 = trim($this->input->post('score3'));

        // if any of them is not empty, that means one or all of them won't be null
        if(($score1 != '') || ($score2 != '') || ($score3 != ''))
        {
            // set the empty ones to null
            $score1 = $score1 == '' ? NULL : (int) $score1;
            $score2 = $score2 == '' ? NULL : (int) $score2;
            $score3 = $score3 == '' ? NULL : (int) $score3;

            $data = array($event_id, $id, $score1, $score2, $score3, (int) $score1 + (int) $score2 + (int) $score3, $created_by );

            $sql = "INSERT INTO scores (event_id, participant_id, score1, score2, score3, result, created_by) VALUES (?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE score1=VALUES(score1), score2=VALUES(score2), score3=VALUES(score3), result=VALUES(result)";
            $this->db->query($sql, $data);
        }

        // if team id is 0 and former team id is not 0, i.e no more in team, delete from team_participants
        if($team_id == 0 && $former_team_id != 0)
        {
            $this->db->delete('team_participants', array('event_id' => $event_id, 'team_id' => $team['id'], 'participant_id' => $id));
        }
        elseif($former_team_id == 0 && $team_id != 0) // just has a team changed, add to team team_participants
        {
            $data = array(
                'event_id' => $event_id,
                'team_id' => $team_id,
                'participant_id' => $id
            );
            $this->db->insert('team_participants', $data);
        }
        elseif($team_id != $former_team_id) // team changed, update to new team
        {
            $data = array(
                'team_id' => $team_id
            );
            $this->db->where('event_id', $event_id);
            $this->db->where('participant_id', $id);
            $this->db->update('team_participants', $data);
        }
        else
        {
            //
        }
    }
}
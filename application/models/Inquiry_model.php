<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Inquiry_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function filter_record_count($user_id = FALSE, $id = FALSE, $title = FALSE, $status_id = FALSE, $date = FALSE)
    {

        $sql = "SELECT COUNT(DISTINCT(inquiries.id)) AS count FROM inquiries LEFT JOIN order_details ON inquiries.id = order_details.order_id LEFT JOIN payments ON inquiries.id = payments.order_id LEFT JOIN users ON inquiries.user_id = users.id WHERE ";

        $where = '';

        if($user_id !== FALSE)
        {

            $user_id = (int) $user_id;
            if($user_id != 0)
            {
                $where .= " AND (inquiries.user_id = '". $user_id . "')";
            }
        }

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (inquiries.id = '". $id . "')";
            }
        }

        if($status_id !== FALSE)
        {
            $status_id = (int) $status_id;
            if($status_id != -1)
            {
                $where .= " AND (inquiries.status_id = '". $status_id . "')";
            }
        }

        if($date !== FALSE)
        {
            $where .= " AND (DATE(inquiries.date_created) = '". $date . "')";
        }

        if($title !== FALSE)
        {
            if(!empty($title))
            {
                // $title = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                // $where .= " AND ((users.full_name LIKE '". $title . "') OR (inquiries.items LIKE '". $title . "'))";

                $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                $where .= " AND ("; 

                $where .= "(users.full_name LIKE '". $title_full . "') OR (products.name LIKE '". $title_full . "')";

                $title_array = explode(' ', $title);
                if(count($title_array) > 1)
                {
                    foreach ($title_array as $title_word) {
                        if(!empty($title_word))
                        {
                            $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                            $where .= " OR (users.full_name LIKE '". $title_word . "') OR (products.name LIKE '". $title_word . "')";
                        }
                    }
                }

                $where .= ")";
            }
        }

        if(empty($where))
        {
            // all null, remove where
            $sql = rtrim($sql, ' WHERE');
        }
        
        // echo $where; die;
        $where = ltrim($where, ' AND');
        // $where .= " GROUP BY inquiries.id, inquiries.user_id, inquiries.question, inquiries.reply, inquiries.total_price, inquiries.status_id, inquiries.date_created, payments.successful, payments.error_code, payments.error_message, users.full_name ";
        $sql = $sql . $where;

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->row_array()['count'];
    }

    public function filter($limit, $offset, $user_id = FALSE, $id = FALSE, $title = FALSE, $status_id = FALSE, $date = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $sql = "SELECT inquiries.id, inquiries.user_id, inquiries.question, inquiries.reply, inquiries.total_price, inquiries.status_id, inquiries.date_created, GROUP_CONCAT(DISTINCT CONCAT(order_details.product_id, '@@@', products.name, '@@@', products.price) ORDER BY order_details.id SEPARATOR '###') AS items, payments.successful, payments.error_code, payments.error_message, users.full_name FROM inquiries LEFT JOIN order_details ON inquiries.id = order_details.order_id LEFT JOIN payments ON inquiries.id = payments.order_id LEFT JOIN users ON inquiries.user_id = users.id WHERE ";

        $where = '';

        if($user_id !== FALSE)
        {

            $user_id = (int) $user_id;
            if($user_id != 0)
            {
                $where .= " AND (inquiries.user_id = '". $user_id . "')";
            }
        }

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (inquiries.id = '". $id . "')";
            }
        }

        if($status_id !== FALSE)
        {
            $status_id = (int) $status_id;
            if($status_id != -1)
            {
                $where .= " AND (inquiries.status_id = '". $status_id . "')";
            }
        }

        if($date !== FALSE)
        {
            $where .= " AND (DATE(inquiries.date_created) = '". $date . "')";
        }

        if($title !== FALSE)
        {
            if(!empty($title))
            {
                // $title = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                // $where .= " AND ((users.full_name LIKE '". $title . "') OR (inquiries.items LIKE '". $title . "'))";

                $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                $where .= " AND ("; 

                $where .= "(users.full_name LIKE '". $title_full . "') OR (products.name LIKE '". $title_full . "')";

                $title_array = explode(' ', $title);
                if(count($title_array) > 1)
                {
                    foreach ($title_array as $title_word) {
                        if(!empty($title_word))
                        {
                            $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                            $where .= " OR (users.full_name LIKE '". $title_word . "') OR (products.name LIKE '". $title_word . "')";
                        }
                    }
                }

                $where .= ")";
            }
        }
        
        if(empty($where))
        {
            // all null, remove where
            $sql = rtrim($sql, ' WHERE');
        }
        
        $where = ltrim($where, ' AND');
        $where .= " GROUP BY inquiries.id, inquiries.user_id, inquiries.question, inquiries.reply, inquiries.total_price, inquiries.status_id, inquiries.date_created, payments.successful, payments.error_code, payments.error_message, users.full_name ";
        $sql = $sql . $where . " ORDER BY inquiries.date_created DESC LIMIT $offset, $limit";

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function record_count($type = "all", $user_id = FALSE)
    {
        if($user_id !== FALSE)
        {
            if($type == "customer")
            {
                $this->db->where('user_id', (int) $user_id);
            }
            $this->db->from('inquiries');
            return $this->db->count_all_results();
        }
        return $this->db->count_all("inquiries");
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if($id === FALSE)
        {
            $limit = (int) $limit;
            $offset = (int) $offset;

            $this->db->order_by('inquiries.date_created', 'DESC');
            $this->db->select("inquiries.id, inquiries.user_id, inquiries.question, inquiries.reply, inquiries.replied_by, inquiries.date_created, users.full_name");
            $this->db->from('inquiries'); 
            $this->db->join('users', 'inquiries.user_id = users.id', 'left');
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();  // echo $this->db->last_query(); die;
            return $query->result_array();
        }

        $this->db->order_by('inquiries.date_created', 'DESC');
        $this->db->select("inquiries.id, inquiries.user_id, inquiries.question, inquiries.reply, inquiries.replied_by, inquiries.date_created, users.full_name");
        $this->db->from('inquiries'); 
        $this->db->join('users', 'inquiries.user_id = users.id', 'left');
        $this->db->where('inquiries.id', (int) $id); 
        $this->db->limit(1);

        $query = $this->db->get(); // echo $this->db->last_query(); die;
        return $query->row_array();
    }

    public function getRowsByUserID($limit, $offset, $user_id)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $this->db->order_by('inquiries.date_created', 'DESC');
        $this->db->select("inquiries.id, inquiries.user_id, inquiries.question, inquiries.reply, inquiries.replied_by, inquiries.date_created");
        $this->db->from('inquiries'); 
        $this->db->where('inquiries.user_id', (int) $user_id); 
        $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

        $query = $this->db->get(); // echo $this->db->last_query(); die;
        return $query->result_array();
    }

    public function add()
    {
        $user_id = (int) $this->session->userdata('user_id');

        $data = array(
            'user_id' => $user_id,
            'question' => trim($this->input->post('question'))
        );

        $this->db->insert('inquiries', $data);
    }

    public function update($id)
    {
        $user_id = (int) $this->session->userdata('user_id');

        $data = array(
            'reply' => trim($this->input->post('reply')),
            'replied_by' => $user_id
        );
        $this->db->where('id', (int) $id);
        $this->db->update('inquiries', $data);
    }

    public function delete($id)
    {
        $this->db->delete('inquiries', array('id' => (int) $id));
    }
}
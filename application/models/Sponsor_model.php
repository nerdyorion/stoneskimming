<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Sponsor_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
        $this->db->reset_query();
    }

    public function filter_record_count($id = FALSE, $title = FALSE)
    {

        $sql = "SELECT COUNT(sponsors.id) AS count FROM sponsors WHERE ";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (sponsors.id = '". $id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(sponsors.first_name LIKE '". $title_full . "') OR (sponsors.last_name LIKE '". $title_full . "') OR (sponsors.company_name LIKE '". $title_full . "') OR (sponsors.phone LIKE '". $title_full . "') OR (sponsors.email LIKE '". $title_full . "') OR (sponsors.gender LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (sponsors.first_name LIKE '". $title_word . "') OR (sponsors.last_name LIKE '". $title_word . "') OR (sponsors.company_name LIKE '". $title_word . "') OR (sponsors.phone LIKE '". $title_word . "') OR (sponsors.email LIKE '". $title_word . "') OR (sponsors.gender LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where;

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->row_array()['count'];
    }

    public function filter($limit, $offset, $id = FALSE, $title = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $sql = "SELECT sponsors.id, sponsors.first_name, sponsors.last_name, sponsors.company_name, sponsors.phone, sponsors.email, sponsors.gender FROM sponsors WHERE ";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (sponsors.id = '". $id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(sponsors.first_name LIKE '". $title_full . "') OR (sponsors.last_name LIKE '". $title_full . "') OR (sponsors.company_name LIKE '". $title_full . "') OR (sponsors.phone LIKE '". $title_full . "') OR (sponsors.email LIKE '". $title_full . "') OR (sponsors.gender LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (sponsors.first_name LIKE '". $title_word . "') OR (sponsors.last_name LIKE '". $title_word . "') OR (sponsors.company_name LIKE '". $title_word . "') OR (sponsors.phone LIKE '". $title_word . "') OR (sponsors.email LIKE '". $title_word . "') OR (sponsors.gender LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where . " ORDER BY sponsors.first_name, sponsors.last_name LIMIT $offset, $limit";

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function record_count()
    {
        return $this->db->count_all("sponsors");
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if ($id === FALSE)
        {
            //$query = $this->db->get('dnd_sponsors');
            //return $query->result_array();
            $query = $this->db->order_by('sponsors.first_name, sponsors.last_name', 'ASC');
            $this->db->select('sponsors.id, sponsors.first_name, sponsors.last_name, sponsors.company_name, sponsors.phone, sponsors.email, sponsors.gender');
            $this->db->from('sponsors');
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get(); //echo $this->db->last_query(); die;
            return $query->result_array();
        }
        $this->db->select('sponsors.id, sponsors.first_name, sponsors.last_name, sponsors.company_name, sponsors.phone, sponsors.email, sponsors.gender');
        $this->db->from('sponsors'); 
        $this->db->where('sponsors.id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();    //echo $this->db->last_query(); die;
        return $query->row_array();
    }

    public function getRowsDropDown()
    {
        $this->db->order_by('sponsors.first_name ASC, sponsors.last_name ASC');
        $this->db->select('sponsors.id, sponsors.first_name, sponsors.last_name, sponsors.company_name, sponsors.phone, sponsors.email, sponsors.gender');
        $this->db->from('sponsors');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function add()
    {
        $created_by = (int) $this->session->userdata('sponsor_id');

        $data = array(
            'first_name' => ucwords(trim($this->input->post('first_name'))),
            'last_name' => ucwords(trim($this->input->post('last_name'))),
            'company_name' => trim($this->input->post('company_name')),
            'phone' => trim($this->input->post('phone')),
            'email' => strtolower(trim($this->input->post('email'))),
            'gender' => trim($this->input->post('gender')),
            'created_by' => $created_by
        );

        $this->db->insert('sponsors', $data);
    }

    public function delete($id)
    {
        $this->db->delete('sponsors', array('id' => (int) $id));
    }

    public function update($id)
    {

        $data = array(
            'first_name' => ucwords(trim($this->input->post('first_name'))),
            'last_name' => ucwords(trim($this->input->post('last_name'))),
            'company_name' => trim($this->input->post('company_name')),
            'phone' => trim($this->input->post('phone')),
            'email' => strtolower(trim($this->input->post('email'))),
            'gender' => trim($this->input->post('gender'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('sponsors', $data);
    }
}
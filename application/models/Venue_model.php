<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Venue_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function record_count() {
        return $this->db->count_all("venues");
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if($id === FALSE)
        {
            $this->db->order_by('name', 'ASC');
            $this->db->select('id, name, capacity, location');
            $this->db->from('venues'); 
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();
            return $query->result_array();
        }

        $this->db->select('id, name, capacity, location');
        $this->db->from('venues');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsDropDown($id = FALSE)
    {
        if ($id === FALSE)
        {
            $this->db->order_by('name', 'ASC');
            $this->db->select('id, name, capacity');
            $this->db->from('venues'); 
            $query = $this->db->get();
            
            return $query->result_array();
        }
        $this->db->select('name, capacity');
        $this->db->from('venues');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRandom($limit)
    {
        $limit = (int) $limit;

        $this->db->order_by('id', 'RANDOM');
        $this->db->select('id, name, capacity');
        $this->db->from('venues');
        $this->db->limit($limit);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function add()
    {
        $created_by = (int) $this->session->userdata('user_id');

        $data = array(
            'name' => trim($this->input->post('name')),
            'capacity' => (int) trim($this->input->post('capacity')),
            'location' => trim($this->input->post('location')),
            'created_by' => $created_by
        );

        $this->db->insert('venues', $data);
    }

    public function delete($id)
    {
        $this->db->delete('venues', array('id' => (int) $id));
    }

    public function update($id)
    {

        $data = array(
            'name' => trim($this->input->post('name')),
            'capacity' => (int) trim($this->input->post('capacity')),
            'location' => trim($this->input->post('location'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('venues', $data);
    }

    public function is_filled($event_id)
    {
        $event_id = (int) $event_id;

        // get venue capacity which determines ticket to be sold
        $this->db->select('venues.capacity');
        $this->db->from('events');
        $this->db->join('venues', 'events.venue_id = venues.id', 'left');
        $this->db->where('events.id', $event_id); 

        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();

        $capacity = $row['capacity'];

        $this->db->reset_query();

        // get registered participants
        $this->db->where('event_id', $event_id);
        $this->db->from('event_participants');
        $participants = $this->db->count_all_results();

        if($participants < $capacity)
        {
            // not yet filled
            return false;
        }
        else
        {
            // venue filled up, no more registering
            return true;
        }
    }
}
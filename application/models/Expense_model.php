<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Expense_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
        $this->db->reset_query();
    }

    public function record_count($event_id = FALSE)
    {
        if($event_id !== FALSE)
        {
            $this->db->where('event_id', (int) $event_id);
            $this->db->from('expenses');
            return $this->db->count_all_results();
        }
        return $this->db->count_all("expenses");
    }

    public function getRows($limit, $offset, $event_id, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $event_id = (int) $event_id;

        if($id === FALSE)
        {
            $this->db->order_by('expenses.name', 'ASC');
            $this->db->select("expenses.id, expenses.event_id, expenses.name, expenses.price, expenses.paid, expenses.created_by, expenses.date_created");
            $this->db->from('expenses');
            $this->db->join('events', 'expenses.event_id = expenses.id', 'left');
            $this->db->where('expenses.event_id', $event_id); 
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();
            return $query->result_array();
        }

        $this->db->select("expenses.id, expenses.event_id, expenses.name, expenses.price, expenses.paid, expenses.created_by, expenses.date_created");
        $this->db->from('expenses');
        $this->db->join('events', 'expenses.event_id = expenses.id', 'left');
        $this->db->where('expenses.id', (int) $id); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsDropDown($event_id)
    {
        $this->db->order_by('teams.name', 'ASC');
        $this->db->select('expenses.id, expenses.event_id, expenses.name, expenses.price, expenses.paid');
        $this->db->from('expenses');
        $this->db->join('events', 'expenses.event_id = expenses.id', 'left');
        $this->db->where('expenses.event_id', (int) $event_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function add($event_id)
    {
        $created_by = (int) $this->session->userdata('user_id');
        $event_id = (int) $event_id;
        $paid = 0;

        if(!is_null($this->input->post('paid')))
        {
            $paid = 1;
        }

        // add to expenses table
        $data = array(
            'event_id' => $event_id,
            'name' => trim($this->input->post('name')),
            'price' => (int) trim($this->input->post('price')),
            'paid' => $paid,
            'created_by' => $created_by
        );

        $this->db->insert('expenses', $data);
    }

    public function delete($event_id, $id)
    {
        $event_id = (int) $event_id;
        $id = (int) $id;

        // delete from expenses
        $this->db->delete('expenses', array('event_id' => $event_id, 'id' => $id));
    }

    public function update($id)
    {
        $paid = 0;

        if(!is_null($this->input->post('paid')))
        {
            $paid = 1;
        }

        $data = array(
            'name' => trim($this->input->post('name')),
            'price' => (int) trim($this->input->post('price')),
            'paid' => $paid
        );
        $this->db->where('id', (int) $id);
        $this->db->update('expenses', $data);
    }
}
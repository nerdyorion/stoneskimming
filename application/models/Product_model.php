<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Product_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function filter_record_count($id = FALSE, $category_id = FALSE, $title = FALSE)
    {

        $sql = "SELECT COUNT(products.id) AS count FROM products LEFT OUTER JOIN categories ON products.category_id = categories.id WHERE ";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (products.id = '". $id . "')";
            }
        }

        if($category_id !== FALSE)
        {
            $category_id = (int) $category_id;
            if($category_id != 0)
            {
                $where .= " AND (products.category_id = '". $category_id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(products.name LIKE '". $title_full . "') OR (products.description LIKE '". $title_full . "') ";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (products.name LIKE '". $title_word . "') OR (products.description LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where;

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->row_array()['count'];
    }

    public function filter($limit, $offset, $id = FALSE, $category_id = FALSE, $title = FALSE, $sort_price = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $sql = "SELECT products.id, products.category_id, products.image_url, products.image_url_thumb, products.name, products.description, categories.name AS category_name, products.price, products.manufactured_date, products.expiry_date, products.featured, (SELECT quantity FROM stock WHERE stock.product_id = products.id) AS count_stock FROM products LEFT OUTER JOIN categories ON products.category_id = categories.id WHERE ";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (products.id = '". $id . "')";
            }
        }

        if($category_id !== FALSE)
        {
            $category_id = (int) $category_id;
            if($category_id != 0)
            {
                $where .= " AND (products.category_id = '". $category_id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(products.name LIKE '". $title_full . "') OR (products.description LIKE '". $title_full . "') ";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (products.name LIKE '". $title_word . "') OR (products.description LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');        
        
        if($sort_price == 'ASC')
        {
            $sql = $sql . $where . " ORDER BY products.price ASC LIMIT $offset, $limit";
        }
        elseif($sort_price == 'DESC')
        {
            $sql = $sql . $where . " ORDER BY products.price DESC LIMIT $offset, $limit";
        }
        else
        {
            $sql = $sql . $where . " ORDER BY products.date_created DESC LIMIT $offset, $limit";
        }

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function record_count($category_id = FALSE) {
        if($category_id !== FALSE)
        {
            $this->db->where('category_id', (int) $category_id);
            $this->db->from('products');
            return $this->db->count_all_results();
        }
        return $this->db->count_all("products");
    }

    public function get_all_slug()
    {
        $sql = "SELECT id, slug FROM products ORDER BY slug ASC";
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getRows($limit, $offset, $id = FALSE, $sort_price = FALSE)
    {
        if ($id === FALSE)
        {   
            $limit = (int) $limit;
            $offset = (int) $offset;

            // var_dump($limit); var_dump($start); die;
            // $this->db->limit($limit, $start);
            $sql = "SELECT products.id, products.category_id, products.image_url, products.image_url_thumb, products.name, categories.name AS category_name, products.price, products.manufactured_date, products.expiry_date, products.featured, (SELECT quantity FROM stock WHERE stock.product_id = products.id) AS count_stock FROM products LEFT JOIN categories ON products.category_id = categories.id";

            if($sort_price == 'ASC')
            {
                $sql .= " ORDER BY products.price ASC LIMIT $offset, $limit";
            }
            elseif($sort_price == 'DESC')
            {
                $sql .= " ORDER BY products.price DESC LIMIT $offset, $limit";
            }
            else
            {
                $sql .= " ORDER BY products.date_created DESC LIMIT $offset, $limit";
            }

            $query = $this->db->query($sql); // var_dump($query); die();
            
            // var_dump($query->result_array()); die();

            return $query->result_array();
        }
        $this->db->select('products.id, products.category_id, products.image_url, products.image_url_thumb, products.name, products.description, categories.name AS category_name, products.price, products.manufactured_date, products.expiry_date, products.featured, (SELECT quantity FROM stock WHERE stock.product_id = products.id) AS count_stock');
        $this->db->from('products'); 
        $this->db->join('categories', 'products.category_id = categories.id');
        $this->db->where('products.id', (int) $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsByCategoryID($limit, $offset, $category_id)
    {

        $limit = (int) $limit;
        $offset = (int) $offset;
        $category_id = (int) $category_id;

        $sql = "SELECT products.id, products.category_id, products.image_url, products.image_url_thumb, products.name, products.description, categories.name AS category_name, products.price, products.manufactured_date, products.expiry_date, products.featured, (SELECT quantity FROM stock WHERE stock.product_id = products.id) AS count_stock FROM products LEFT OUTER JOIN categories ON products.category_id = categories.id WHERE products.category_id = '$category_id' ORDER BY products.date_created DESC LIMIT $offset, $limit";

        $query = $this->db->query($sql); // echo $this->db->last_query(); die;

        return $query->result_array();
    }

    public function getHotOffers($limit)
    {
        $limit = (int) $limit;

        $sql = "SELECT products.id, products.category_id, products.image_url, products.image_url_thumb, products.name, products.description, categories.name AS category_name, products.price, products.manufactured_date, products.expiry_date, products.featured, (SELECT quantity FROM stock WHERE stock.product_id = products.id) AS count_stock FROM products LEFT JOIN categories ON products.category_id = categories.id ORDER BY products.price ASC LIMIT $limit";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getFeatured($limit)
    {
        $limit = (int) $limit;

        $sql = "SELECT products.id, products.category_id, products.image_url, products.image_url_thumb, products.name, products.description, categories.name AS category_name, products.price, products.manufactured_date, products.expiry_date, products.featured, (SELECT quantity FROM stock WHERE stock.product_id = products.id) AS count_stock FROM products LEFT JOIN categories ON products.category_id = categories.id WHERE products.featured = 1 ORDER BY products.price DESC LIMIT $limit";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getRowsDropDown()
    {
        $this->db->select('id, name, active');
        $this->db->from('products');
        $this->db->where('active', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAllRows()
    {
        $sql = "SELECT products.id, products.image_url, products.image_url_thumb, products.name, categories.name AS category_name, products.price, products.manufactured_date, products.expiry_date, products.featured, (SELECT quantity FROM stock WHERE stock.product_id = products.id) AS count_stock  FROM products LEFT JOIN categories ON products.category_id = categories.id ORDER BY products.name ASC";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getRowsFrontEnd($limit, $offset, $category_id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if ($category_id === FALSE)
        {
            $sql = "SELECT products.id, products.image_url, products.image_url_thumb, products.name, products.description, categories.name AS category_name, products.price, products.manufactured_date, products.expiry_date, products.featured, products.mtn_service_code, products.mtn_service_keyword, products.airtel_service_code, products.airtel_service_keyword, products.glo_service_code, products.glo_service_keyword, products.emts_service_code, products.emts_service_keyword, products.ntel_service_code, products.ntel_service_keyword, (SELECT quantity FROM stock WHERE stock.product_id = products.id) AS count_stock  FROM products LEFT JOIN categories ON products.category_id = categories.id WHERE products.active = 1 ORDER BY RAND() LIMIT $offset, $limit";

            $query = $this->db->query($sql); // var_dump($query); die();

            return $query->result_array();
        }

        $category_id = (int) $category_id;

        $sql = "SELECT products.id, products.image_url, products.image_url_thumb, products.name, products.description, categories.name AS category_name, products.price, products.manufactured_date, products.expiry_date, products.featured, products.mtn_service_code, products.mtn_service_keyword, products.airtel_service_code, products.airtel_service_keyword, products.glo_service_code, products.glo_service_keyword, products.emts_service_code, products.emts_service_keyword, products.ntel_service_code, products.ntel_service_keyword, (SELECT quantity FROM stock WHERE stock.product_id = products.id) AS count_stock  FROM products LEFT JOIN categories ON products.category_id = categories.id WHERE products.category_id = '$category_id' AND products.active = 1 ORDER BY RAND() LIMIT $offset, $limit";
        $query = $this->db->query($sql); // var_dump($query); die();

        return $query->result_array();
    }

    public function getRowsFrontEndSingleCategory($limit, $offset, $category_id)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $category_id = (int) $category_id;

        $sql = "SELECT products.id, products.image_url, products.image_url_thumb, products.name, products.description, categories.name AS category_name, products.price, products.manufactured_date, products.expiry_date, products.featured, products.mtn_service_code, products.mtn_service_keyword, products.airtel_service_code, products.airtel_service_keyword, products.glo_service_code, products.glo_service_keyword, products.emts_service_code, products.emts_service_keyword, products.ntel_service_code, products.ntel_service_keyword, (SELECT quantity FROM stock WHERE stock.product_id = products.id) AS count_stock  FROM products LEFT JOIN categories ON products.category_id = categories.id WHERE products.category_id = '$category_id' AND products.active = 1 ORDER BY products.name LIMIT $offset, $limit";
        $query = $this->db->query($sql); // var_dump($query); die();

        return $query->result_array();
    }

    public function add()
    {
        $created_by = (int) $this->session->userdata('user_id');

        $data = array(
            'category_id' => (int) $this->input->post('category_id'),
            'name' => trim($this->input->post('name')),
            'description' => trim($this->input->post('description')),
            'price' => (int) $this->input->post('price'),
            'image_url' => $this->input->post('image_url'),
            'image_url_thumb' => $this->input->post('image_url_thumb'),
            'manufactured_date' => trim($this->input->post('manufactured_date')),
            'expiry_date' => trim($this->input->post('expiry_date')),
            'created_by' => $created_by
        );

        $this->db->insert('products', $data);

        // add stock
        $product_id = $this->db->insert_id();
        $data = array(
            'product_id' => (int) $product_id,
            'quantity' => (int) $this->input->post('stock')
        );
        $this->db->insert('stock', $data);
    }

    public function delete($id)
    {
        $this->db->delete('products', array('id' => (int) $id));
    }

    public function update($id)
    {

        $data = array(
            'category_id' => (int) $this->input->post('category_id'),
            'name' => trim($this->input->post('name')),
            'description' => trim($this->input->post('description')),
            'price' => (int) $this->input->post('price'),
            'image_url' => $this->input->post('image_url'),
            'image_url_thumb' => $this->input->post('image_url_thumb'),
            'manufactured_date' => trim($this->input->post('manufactured_date')),
            'expiry_date' => trim($this->input->post('expiry_date'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('products', $data);

        // update stock
        $data = array(
            'quantity' => (int) $this->input->post('stock')
        );
        $this->db->where('product_id', (int) $id);
        $this->db->update('stock', $data);
    }

    public function updateWithoutImage($id)
    {

        $data = array(
            'category_id' => (int) $this->input->post('category_id'),
            'name' => trim($this->input->post('name')),
            'description' => trim($this->input->post('description')),
            'price' => (int) $this->input->post('price'),
            'manufactured_date' => trim($this->input->post('manufactured_date')),
            'expiry_date' => trim($this->input->post('expiry_date'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('products', $data);

        // update stock
        $data = array(
            'quantity' => (int) $this->input->post('stock')
        );
        $this->db->where('product_id', (int) $id);
        $this->db->update('stock', $data);
    }

    public function setFeatured($id, $featured)
    {
        $data = array(
            'featured' => (int) $featured
        );
        $this->db->where('id', (int) $id);
        $this->db->update('products', $data);
    }

    public function enable($id)
    {
        $data = array(
            'active' => 1
        );
        $this->db->where('id', (int) $id);
        $this->db->update('products', $data);
    }

    public function disable($id)
    {
        $data = array(
            'active' => 0
        );
        $this->db->where('id', (int) $id);
        $this->db->update('products', $data);
    }

    public function slugExists($slug, $id = FALSE)
    {
        if ($id === FALSE)
        {
            $this->db->select('id, slug');
            $this->db->from('products');
            $this->db->like('slug', $slug, 'after');
            $query = $this->db->order_by('id', 'DESC');
            $query = $this->db->limit(1);
            $query = $this->db->get();

            $row = $query->row_array();

            if(empty($row))
            {
                return false;
            }
            else
            {
                return $row['slug'];
            }
        }

        $this->db->select('id, slug');
        $this->db->from('products');
        $this->db->where('id !=', $id); 
        $this->db->like('slug', $slug, 'after');
        $query = $this->db->order_by('id', 'DESC');
        $query = $this->db->limit(1);
        $query = $this->db->get();

        $row = $query->row_array();

        if(empty($row))
        {
            return false;
        }
        else
        {
            return $row['slug'];
        }
    }
}
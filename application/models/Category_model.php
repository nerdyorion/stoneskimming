<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Category_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function record_count() {
        return $this->db->count_all("categories");
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if($id === FALSE)
        {
            $this->db->order_by('name', 'ASC');
            $this->db->select('id, name, description');
            $this->db->from('categories'); 
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();
            return $query->result_array();
        }

        $this->db->select('id, name, description');
        $this->db->from('categories');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsDropDown($id = FALSE)
    {
        if ($id === FALSE)
        {
            $this->db->order_by('name', 'ASC');
            $this->db->select('id, name, description');
            $this->db->from('categories'); 
            $query = $this->db->get();
            
            return $query->result_array();
        }
        $this->db->select('name');
        $this->db->from('categories');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRandom($limit)
    {
        $limit = (int) $limit;

        $this->db->order_by('id', 'RANDOM');
        $this->db->select('id, name');
        $this->db->from('categories');
        $this->db->limit($limit);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function add()
    {
        $created_by = (int) $this->session->userdata('user_id');

        $data = array(
            'name' => trim($this->input->post('name')),
            'description' => trim($this->input->post('description')),
            'created_by' => $created_by
        );

        $this->db->insert('categories', $data);
    }

    public function delete($id)
    {
        $this->db->delete('categories', array('id' => (int) $id));
    }

    public function update($id)
    {

        $data = array(
            'name' => trim($this->input->post('name')),
            'description' => trim($this->input->post('description'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('categories', $data);
    }

    public function exists($id)
    {
        $this->db->select('name');
        $this->db->from('categories');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if(empty($row))
        {
            // id does not exist
            return false;
        }
        else
        {
            // id exist
            return $row['name'];
        }
    }
}
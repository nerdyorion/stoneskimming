<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Ticket_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function record_count() {
        return $this->db->count_all("tickets");
    }

    public function boughtTicketsRevenueCurrentYear()
    {
        $sql = "SELECT SUM(bought_tickets.price) AS sum FROM `bought_tickets` LEFT OUTER JOIN `events` ON bought_tickets.event_id = events.id WHERE events.year = YEAR(CURRENT_DATE)";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['sum'];
    }

    public function countBoughtTicketsCurrentYear()
    {
        $sql = "SELECT COUNT(bought_tickets.participant_id) AS count FROM `bought_tickets` LEFT OUTER JOIN `events` ON bought_tickets.event_id = events.id WHERE events.year = YEAR(CURRENT_DATE)";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['count'];
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if($id === FALSE)
        {
            $this->db->order_by('tickets.event_id', 'DESC');
            $this->db->select('tickets.id, tickets.event_id, tickets.category_id, tickets.price_per_ticket, tickets.tickets_sold, events.name AS event_name, events.year AS event_year, categories.name AS category_name');
            $this->db->from('tickets'); 
            $this->db->join('events', 'tickets.event_id = events.id', 'left');
            $this->db->join('categories', 'tickets.category_id = categories.id', 'left');
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();
            return $query->result_array();
        }

        $this->db->select('tickets.id, tickets.event_id, tickets.category_id, tickets.price_per_ticket, tickets.tickets_sold, events.name AS event_name, events.year AS event_year, categories.name AS category_name');
        $this->db->from('tickets');
        $this->db->join('events', 'tickets.event_id = events.id', 'left');
        $this->db->join('categories', 'tickets.category_id = categories.id', 'left');
        $this->db->where('tickets.id', $id); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsDropDown()
    {
        $this->db->order_by('tickets.event_id', 'DESC');
        $this->db->select('tickets.id, tickets.event_id, tickets.category_id, tickets.price_per_ticket, tickets.tickets_sold, events.name AS event_name, events.year AS event_year, categories.name AS category_name');
        $this->db->from('tickets'); 
        $this->db->join('events', 'tickets.event_id = events.id', 'left');
        $this->db->join('categories', 'tickets.category_id = categories.id', 'left');
        $query = $this->db->get();

        return $query->result_array();
    }

    public function add()
    {
        $created_by = (int) $this->session->userdata('user_id');

        $data = array(
            'event_id' => (int) trim($this->input->post('event_id')),
            'category_id' => (int) trim($this->input->post('category_id')),
            'price_per_ticket' => (float) trim($this->input->post('price_per_ticket')),
            'created_by' => $created_by
        );

        $this->db->insert('tickets', $data);
    }

    public function delete($id)
    {
        $this->db->delete('tickets', array('id' => (int) $id));
    }

    public function update($id)
    {
        $data = array(
            'event_id' => (int) trim($this->input->post('event_id')),
            'category_id' => (int) trim($this->input->post('category_id')),
            'price_per_ticket' => (float) trim($this->input->post('price_per_ticket'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('tickets', $data);
    }

    public function markPaid($event_id, $participant_id, $category_id, $price)
    {
        $created_by = (int) $this->session->userdata('user_id');
        
        $event_id = (int) $event_id;
        $participant_id = (int) $participant_id;
        $category_id = (int) $category_id;
        $price = (float) $price;

        $data = array(
            'participant_id' => $participant_id,
            'event_id' => $event_id,
            'price' => $price,
            'created_by' => $created_by
        );
        $this->db->insert('bought_tickets', $data);
        $this->db->reset_query();

        // add 1 to tickets_sold column in tickets table
        $sql = "UPDATE tickets SET tickets_sold = tickets_sold + 1 WHERE event_id = '$event_id' AND category_id = '$category_id'";
        $this->db->query($sql);

        // update event_participant status to paid (1)
        $data = array(
            'paid' => '1',
        );
        $this->db->where('event_id', $event_id);
        $this->db->where('participant_id', $participant_id);
        $this->db->update('event_participants', $data);
    }

    public function markUnpaid($event_id, $participant_id, $category_id)
    {
        $event_id = (int) $event_id;
        $participant_id = (int) $participant_id;
        $category_id = (int) $category_id;

        $this->db->delete('bought_tickets', array('event_id' => $event_id, 'participant_id' => $participant_id));
        $this->db->reset_query();

        // remove 1 from tickets_sold column in tickets table
        $sql = "UPDATE tickets SET tickets_sold = tickets_sold - 1 WHERE (event_id = '$event_id' AND category_id = '$category_id') AND (tickets_sold != 0)";
        $this->db->query($sql);

        // update event_participant status to unpaid (0)
        $data = array(
            'paid' => '0',
        );
        $this->db->where('event_id', $event_id);
        $this->db->where('participant_id',  $participant_id);
        $this->db->update('event_participants', $data);
    }

    public function priceExistsForCategory($event_id, $category_id, $ticket_id = FALSE)
    {
        $event_id = (int) $event_id;
        $category_id = (int) $category_id;

        $this->db->where('event_id', $event_id);
        $this->db->where('category_id', $category_id);
        if($ticket_id !== FALSE)
        {
            $this->db->where('id !=', (int) $ticket_id);
        }
        $this->db->from('tickets');
        $count = $this->db->count_all_results();

        if($count < 1)
        {
            // ticket not yet added for event and category 
            return false;
        }
        else
        {
            // ticket already added for event and category 
            return true;
        }
    }

    public function getPrice($event_id, $category_id)
    {
        $event_id = (int) $event_id;
        $category_id = (int) $category_id;

        // get ticket price for event and category
        $this->db->select('price_per_ticket');
        $this->db->from('tickets');
        $this->db->where('event_id', $event_id); 
        $this->db->where('category_id', $category_id); 

        $query = $this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();

        $price = $row['price_per_ticket'];

        return $price;
    }
}
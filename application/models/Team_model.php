<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Team_model extends CI_Model {

    private $team_size = 4;

    public function __construct()
    {
        $this->load->database();
        $this->db->reset_query();
    }

    public function countCurrentYear()
    {
        $sql = "SELECT COUNT(event_teams.team_id) AS count FROM `event_teams` LEFT OUTER JOIN `events` ON event_teams.event_id = events.id WHERE events.year = YEAR(CURRENT_DATE)";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['count'];
    }

    public function record_count($event_id = FALSE)
    {
        if($event_id !== FALSE)
        {
            $this->db->where('event_id', (int) $event_id);
            $this->db->from('event_teams');
            return $this->db->count_all_results();
        }
        return $this->db->count_all("event_teams");
    }

    public function getRows($limit, $offset, $event_id, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $event_id = (int) $event_id;

        // increase group concat_length
        $sql = "SET SESSION group_concat_max_len = 100000;";
        $this->db->query($sql);
        $this->db->reset_query();

        if($id === FALSE)
        {
            $this->db->order_by('teams.name', 'ASC');
            $this->db->select("teams.id, teams.name, teams.description, teams.created_by, teams.date_created, (SELECT COUNT(team_participants.participant_id) FROM team_participants WHERE team_participants.team_id = teams.id) AS participants, (SELECT GROUP_CONCAT(CONCAT_WS('@@@@@', participants.id, participants.first_name, participants.last_name, participants.age, participants.gender) ORDER BY participants.first_name ASC, participants.last_name ASC SEPARATOR '#####') FROM team_participants LEFT OUTER JOIN participants ON team_participants.participant_id = participants.id WHERE team_participants.team_id = teams.id) AS participants_details");
            $this->db->from('event_teams');
            $this->db->join('teams', 'event_teams.team_id = teams.id', 'left');
            $this->db->where('event_teams.event_id', $event_id); 
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();
            return $query->result_array();
        }

        $this->db->select("teams.id, teams.name, teams.description, teams.created_by, teams.date_created, (SELECT COUNT(team_participants.participant_id) FROM team_participants WHERE team_participants.team_id = teams.id) AS participants, (SELECT GROUP_CONCAT(CONCAT_WS('@@@@@', participants.id, participants.first_name, participants.last_name, participants.age, participants.gender) ORDER BY participants.first_name ASC, participants.last_name ASC SEPARATOR '#####') FROM team_participants LEFT OUTER JOIN participants ON team_participants.participant_id = participants.id WHERE team_participants.team_id = teams.id) AS participants_details");
        $this->db->from('event_teams');
        $this->db->join('teams', 'event_teams.team_id = teams.id', 'left');
        $this->db->where('teams.id', (int) $id); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsDropDown($event_id)
    {
        $this->db->order_by('teams.name', 'ASC');
        $this->db->select('teams.id, teams.name, (SELECT COUNT(team_participants.participant_id) FROM team_participants WHERE team_participants.team_id = teams.id) AS participants');
        $this->db->from('event_teams');
        $this->db->join('teams', 'event_teams.team_id = teams.id', 'left');
        $this->db->where('event_teams.event_id', (int) $event_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getRowsDropDown_NotYetFull($event_id)
    {
        $this->db->order_by('teams.name', 'ASC');
        $this->db->select('teams.id, teams.name, (SELECT COUNT(team_participants.participant_id) FROM team_participants WHERE team_participants.team_id = teams.id) AS participants');
        $this->db->from('event_teams');
        $this->db->join('teams', 'event_teams.team_id = teams.id', 'left');
        $this->db->where('event_teams.event_id', (int) $event_id);
        $this->db->where('(SELECT COUNT(team_participants.participant_id) FROM team_participants WHERE team_participants.team_id = teams.id) <', $this->team_size);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getRowsDropdown_TeamsBeforeYear($event_year)
    {
        $this->db->order_by('events.year', 'DESC');
        $this->db->order_by('teams.name', 'ASC');
        $this->db->select('teams.id, teams.name, events.year');
        $this->db->from('event_teams');
        $this->db->join('teams', 'event_teams.team_id = teams.id', 'left');
        $this->db->join('events', 'event_teams.event_id = events.id', 'left');
        $this->db->where('events.year <', (int) $event_year);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function add($event_id)
    {
        $created_by = (int) $this->session->userdata('user_id');
        $event_id = (int) $event_id;

        // add to teams table
        $data = array(
            'name' => trim($this->input->post('name')),
            'description' => trim($this->input->post('description')),
            'created_by' => $created_by
        );

        $this->db->insert('teams', $data);

        $team_id = $this->db->insert_id();

        // add to event_teams table
        $data = array(
            'event_id' => $event_id,
            'team_id' => (int) $team_id
        );
        $this->db->insert('event_teams', $data);
    }

    public function exists($event_id, $team_id)
    {
        $event_id = (int) $event_id;
        $team_id = (int) $team_id;
        
        $this->db->where('event_id', $event_id);
        $this->db->where('team_id', $team_id);
        $this->db->from('event_teams');
        $count = $this->db->count_all_results();

        if($count < 1)
        {
            // team not yet added for event
            return false;
        }
        else
        {
            // team already added for event
            return true;
        }
    }

    public function nameExists($event_id, $team_name)
    {
        $event_id = (int) $event_id;
        
        $this->db->where('event_teams.event_id', $event_id);
        $this->db->where('teams.name', $team_name);
        $this->db->join('teams', 'event_teams.team_id = teams.id', 'left');
        $this->db->from('event_teams');
        $count = $this->db->count_all_results();

        if($count < 1)
        {
            // team name not yet added for event
            return false;
        }
        else
        {
            // team name already added for event
            return true;
        }
    }

    public function reusePastTeam($event_id)
    {
        $created_by = (int) $this->session->userdata('user_id');
        $event_id = (int) $event_id;

        // add to event_teams table
        $data = array(
            'event_id' => $event_id,
            'team_id' => (int) trim($this->input->post('former_team_id'))
        );
        $this->db->insert('event_teams', $data);
    }

    public function delete($event_id, $id)
    {
        $event_id = (int) $event_id;
        $id = (int) $id;

        // delete from event_teams
        $this->db->delete('event_teams', array('event_id' => $event_id, 'team_id' => $id));

        // delete from team_participants
        $this->db->delete('team_participants', array('team_id' => $id));

        // if team is not used in any other event, you can delete from the main teams table
        $this->db->reset_query();
        $this->db->where('team_id', $id);
        $this->db->from('event_teams');
        $team = $this->db->count_all_results();

        if((int) $team < 1)
        {
            // not used in any other event, so delete from teams table
            $this->db->delete('teams', array('id' => (int) $id));
        }
        else
        {
            // used in another event, so leave it
        }
    }

    public function update($id)
    {
        $data = array(
            'name' => trim($this->input->post('name')),
            'description' => trim($this->input->post('description'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('teams', $data);
    }

    public function is_filled($event_id, $id)
    {
        $this->db->where('event_id', (int) $event_id);
        $this->db->where('team_id', (int) $event_id);
        $this->db->from('team_participants');
        $participants = $this->db->count_all_results();

        if($participants < $this->team_size)
        {
            // not yet filled
            return false;
        }
        else
        {
            // team filled up, no more registering in team
            return true;
        }
    }
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Event_Sponsor_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
        $this->db->reset_query();
    }

    public function record_count($event_id = FALSE)
    {
        if($event_id !== FALSE)
        {
            $this->db->where('event_id', (int) $event_id);
            $this->db->from('event_sponsors');
            return $this->db->count_all_results();
        }
        return $this->db->count_all("event_sponsors");
    }

    public function getRows($limit, $offset, $event_id, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $event_id = (int) $event_id;

        if($id === FALSE)
        {
            $this->db->order_by('sponsors.first_name ASC, sponsors.last_name ASC');
            $this->db->select("event_sponsors.event_id, event_sponsors.sponsor_id, event_sponsors.amount, event_sponsors.created_by, event_sponsors.date_created, sponsors.first_name, sponsors.last_name, sponsors.company_name");
            $this->db->from('event_sponsors');
            $this->db->join('sponsors', 'event_sponsors.sponsor_id = sponsors.id', 'left');
            $this->db->where('event_sponsors.event_id', $event_id); 
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();
            return $query->result_array();
        }

        $this->db->select("event_sponsors.event_id, event_sponsors.sponsor_id, event_sponsors.amount, event_sponsors.created_by, event_sponsors.date_created, sponsors.first_name, sponsors.last_name, sponsors.company_name");
        $this->db->from('event_sponsors');
        $this->db->join('sponsors', 'event_sponsors.sponsor_id = sponsors.id', 'left');
        $this->db->where('event_sponsors.event_id', $event_id); 
        $this->db->where('event_sponsors.sponsor_id', (int) $id); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsDropDown($event_id)
    {
        $this->db->order_by('sponsors.first_name ASC, sponsors.last_name ASC');
        $this->db->select('event_sponsors.event_id, event_sponsors.sponsor_id, event_sponsors.amount, sponsors.first_name, sponsors.last_name, sponsors.company_name');
        $this->db->from('event_sponsors');
        $this->db->join('sponsors', 'event_sponsors.sponsor_id = sponsors.id', 'left');
        $this->db->where('event_sponsors.event_id', (int) $event_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function exists($event_id, $sponsor_id)
    {
        $event_id = (int) $event_id;
        $sponsor_id = (int) $sponsor_id;
        
        $this->db->where('event_id', $event_id);
        $this->db->where('sponsor_id', $sponsor_id);
        $this->db->from('event_sponsors');
        $count = $this->db->count_all_results();

        if($count < 1)
        {
            // sponsor not yet added for event
            return false;
        }
        else
        {
            // sponsor already added for event
            return true;
        }
    }

    public function add($event_id)
    {
        $created_by = (int) $this->session->userdata('user_id');
        $event_id = (int) $event_id;

        // add to event_sponsors table
        $data = array(
            'event_id' => $event_id,
            'sponsor_id' => (int) trim($this->input->post('sponsor_id')),
            'amount' => (int) trim($this->input->post('amount')),
            'created_by' => $created_by
        );

        $this->db->insert('event_sponsors', $data);
    }

    public function delete($event_id, $id)
    {
        $event_id = (int) $event_id;
        $id = (int) $id;

        // delete from event_sponsors
        $this->db->delete('event_sponsors', array('event_id' => $event_id, 'sponsor_id' => $id));
    }

    public function update($event_id, $id)
    {
        $data = array(
            'amount' => (int) trim($this->input->post('amount'))
        );
        $this->db->where('event_id', (int) $event_id);
        $this->db->where('sponsor_id', (int) $id);
        $this->db->update('event_sponsors', $data);
    }
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Event_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function record_count() {
        return $this->db->count_all("events");
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if($id === FALSE)
        {
            $this->db->order_by('year', 'DESC');
            $this->db->select('events.id, events.name, events.year, events.date, events.time, events.venue_id, venues.name AS venue_name, venues.capacity AS venue_capacity, (SELECT COUNT(event_participants.participant_id) FROM event_participants WHERE event_participants.event_id = events.id) AS participants, (SELECT COUNT(event_teams.event_id) FROM event_teams WHERE event_teams.event_id = events.id) AS teams, (SELECT COUNT(expenses.event_id) FROM expenses WHERE expenses.event_id = events.id) AS expenses, (SELECT COUNT(event_sponsors.event_id) FROM event_sponsors WHERE event_sponsors.event_id = events.id) AS event_sponsors');
            $this->db->from('events'); 
            $this->db->join('venues', 'events.venue_id = venues.id', 'left');
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();
            return $query->result_array();
        }

        $this->db->select('events.id, events.name, events.year, events.date, events.time, events.venue_id, venues.name AS venue_name, venues.capacity AS venue_capacity, (SELECT COUNT(event_participants.participant_id) FROM event_participants WHERE event_participants.event_id = events.id) AS participants, (SELECT COUNT(event_teams.event_id) FROM event_teams WHERE event_teams.event_id = events.id) AS teams, (SELECT COUNT(expenses.event_id) FROM expenses WHERE expenses.event_id = events.id) AS expenses, (SELECT COUNT(event_sponsors.event_id) FROM event_sponsors WHERE event_sponsors.event_id = events.id) AS event_sponsors');
        $this->db->from('events');
        $this->db->join('venues', 'events.venue_id = venues.id', 'left');
        $this->db->where('events.id', (int) $id); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsDropDown($id = FALSE)
    {
        if ($id === FALSE)
        {
            $this->db->order_by('year', 'DESC');
            $this->db->select('id, name, year');
            $this->db->from('events'); 
            $query = $this->db->get();
            
            return $query->result_array();
        }
        $this->db->select('name, year');
        $this->db->from('events');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsDropdown_EventsBeforeYear($event_year)
    {
        $this->db->order_by('year', 'DESC');
        $this->db->select('id, name, year');
        $this->db->from('events');
        $this->db->where('year <', (int) $event_year);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function add()
    {
        $created_by = (int) $this->session->userdata('user_id');
        $date = trim($this->input->post('date'));

        $data = array(
            'name' => trim($this->input->post('name')),
            'year' => intval($date),
            'date' => $date,
            'time' => trim($this->input->post('time')),
            'venue_id' => (int) trim($this->input->post('venue_id')),
            'created_by' => $created_by
        );

        $this->db->insert('events', $data);
    }

    public function delete($id)
    {
        // delete event
        $this->db->delete('events', array('id' => (int) $id));

        // delete event participants
        $this->db->delete('event_participants', array('event_id' => (int) $id));

        // delete event tickets
        $this->db->delete('tickets', array('event_id' => (int) $id));
        $this->db->delete('bought_tickets', array('event_id' => (int) $id));

        // delete event sponsors
        $this->db->delete('event_sponsors', array('event_id' => (int) $id));

        // delete event expenses
        $this->db->delete('expenses', array('event_id' => (int) $id));

        // delete event scores
        $this->db->delete('scores', array('event_id' => (int) $id));

        // delete event teams
        $this->db->delete('event_teams', array('event_id' => (int) $id));
        $this->db->delete('team_participants', array('event_id' => (int) $id));
    }

    public function update($id)
    {
        $date = trim($this->input->post('date'));

        $data = array(
            'name' => trim($this->input->post('name')),
            'year' => intval($date),
            'date' => $date,
            'time' => trim($this->input->post('time')),
            'venue_id' => (int) trim($this->input->post('venue_id'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('events', $data);
    }
}
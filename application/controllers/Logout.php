<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	public function index()
	{
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('user_full_name');
        $this->session->unset_userdata('user_email');
		$this->session->unset_userdata('user_role_id');
		$this->session->unset_userdata('user_role_name');
		$this->session->unset_userdata('user_last_login');
        redirect('/login', 'refresh');
	}
}

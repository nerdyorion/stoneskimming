<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_Complete extends CI_Controller {

    public function __construct()
    {
            parent::__construct();

            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/login');
            }
            $this->load->model('User_model');

            // NB: Roles
            // Role ID 1 - Super Administrator
            // Role ID 2 - Administrator
            // Role ID 3 - Student
    }

    public function index()
    {
        $header['page_title'] = 'Order Complete';
        $header['page_desc'] = $header['page_title'] .' | Buy Chocolate Cakes and other variants.';
        $header['categories_all'] = $this->Category_model->getRowsDropDown();

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $data['rows'] = '';
        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('order-complete', $data);  // load content view
    }
}

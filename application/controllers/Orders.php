<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if(!is_logged_in())  // check if logged in
        {
            //redirect to login
            redirect('/login');
        }

        $this->load->model('User_model');
        $this->load->model('Order_model');
        $this->load->library("pagination");
    }

    public function index()
    {
        $user_id = (int) $this->session->userdata("user_id");

        $header['page_title'] = 'My Orders';
        $header['page_desc'] = $header['page_title'] .' | Buy Chocolate Cakes and other variants.';
        $header['categories_all'] = $this->Category_model->getRowsDropDown();
        
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        // Pagination
        $config["base_url"] = base_url() . "orders/index";
        $config["total_rows"] = $this->Order_model->record_count("customer", $user_id);
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;
        $config['use_page_numbers']  = TRUE;
        $config['reuse_query_string']=TRUE;

        // Bootstrap 4 Pagination fix
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
        // $config['next_tag_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tag_close']  = '</span></li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tag_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tag_close']  = '</span></li>';

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        $data["rows"] = $this->Order_model->getRowsByUserID($config["per_page"], $offset, $user_id);
        $data["links"] = $this->pagination->create_links();
    
        // echo "<pre>"; var_dump($data["rows"]); die;

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('orders', $data);  // load content view
    }

    public function download($id)
    {
        $user_id = (int) $this->session->userdata("user_id");

        $id = (int) $id;
    
        $this->load->library('zip');
        // $this->load->helper('download');
        
        $rows = $this->Order_model->getOrderItemsByUserID($user_id, $id);

        // echo '<pre>'; var_dump($rows); die;

        $path = $this->upload_save_path;

        if(!empty($rows))
        {
            // force_download($path . $data['row']['file_url'], NULL, TRUE);

            foreach ($rows as $row)
            {
                $this->Download_model->add($row['item_id'], 1); // 1 for paid past_exam_solution
                $this->zip->read_file($path . $row['file_url']);
            }

            $this->zip->download('order_#' . $id . '_academicianhelp.zip');
        }
        else
        {
            $this->session->set_flashdata('error_code', 1);
            $this->session->set_flashdata('error', "Unable to download file(s)! Please try again later.");
            redirect('/orders', 'refresh');
        }
    }
}
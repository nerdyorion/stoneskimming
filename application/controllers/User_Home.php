<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User_Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if(!is_logged_in())  // check if logged in
        {
            //redirect to login
            redirect('/login');
        }

        $this->load->model('User_model');
        $this->load->model('Inquiry_model');
        $this->load->model('Order_model');
        $this->load->library("pagination");
    }

    public function index()
    {
        $user_id = (int) $this->session->userdata("user_id");

        $full_name = $this->session->userdata("user_full_name");

        $header['page_title'] = "Welcome $full_name";
        $header['page_desc'] = $header['page_title'] .' | Buy Chocolate Cakes and other variants.';
        $header['categories_all'] = $this->Category_model->getRowsDropDown();

        $data['orders_total'] = (int) $this->Order_model->record_count('customer', $user_id);
        $data['inquiries_total'] = (int) $this->Inquiry_model->record_count('customer', $user_id);
        
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
    
        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('user-home', $data);  // load content view
    }

    public function switchbacktoadmin($id) // log back in to admin account
    {
        $id = (int) $id;
        $data['row'] = $this->User_model->loginAs($id, true); // second param siginifies loggin back as admin
        if($data['row'])
        {
            redirect('/admin123', 'refresh');
        }
        else
        {
            $this->session->set_flashdata('error_code', 1);
            $this->session->set_flashdata('error', "Unable to grant access. Please try again.");
            redirect('/dashboard', 'refresh');
        }
    }
}
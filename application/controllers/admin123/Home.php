<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller {

    public function __construct()
    {
        // var_dump("hello"); die;
            parent::__construct();
            // $this->load->library('session');
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            $this->load->model('Participant_model');
            $this->load->model('Ticket_model');
            $this->load->model('Team_model');
    }

	public function index()
	{
        $header['page_title'] = 'Home';
        $data['sn'] = 1;
        $data['participants'] = (int) $this->Participant_model->countCurrentYear();
        $data['bought_tickets_revenue'] = (int) $this->Ticket_model->boughtTicketsRevenueCurrentYear();
        $data['bought_tickets'] = (int) $this->Ticket_model->countBoughtTicketsCurrentYear();
        $data['teams'] = (int) $this->Team_model->countCurrentYear();

        $data['last_login'] = $this->session->userdata("user_last_login");
        // $data["rows"] = $this->Past_Exam_Solution_model->getRowsAllUnpaid(5, 0);
        $data["rows"] = array();
        //$data['rows'] = 'rows';
        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_admin') . 'home', $data);  // load content view
	}
}
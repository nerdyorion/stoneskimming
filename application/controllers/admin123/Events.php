<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends Admin_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            $this->load->model('Event_model');
            $this->load->model('Venue_model');
            $this->load->library("pagination");
    }

    public function index()
    {
        $user_id = (int) $this->session->userdata("user_id");

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        $header['page_title'] = 'Events';

        $data['venues'] = $this->Venue_model->getRowsDropdown();
        $data['event_time'] = $this->event_time();

        // Pagination
        $config["base_url"] = base_url() . "admin123/events/index";
        $config["total_rows"] = $this->Event_model->record_count();
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;
        $config['use_page_numbers']  = TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        $data["rows"] = $this->Event_model->getRows($config["per_page"], $offset);
        $data["links"] = $this->pagination->create_links();

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);
        $this->load->view($this->config->item('template_dir_admin') . 'menu');
        $this->load->view($this->config->item('template_dir_admin') . 'events', $data);
    }

    public function create()
    {
        $user_id = (int) $this->session->userdata("user_id");

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Event Name', 'trim|required|max_length[2000]');
        $this->form_validation->set_rules('date', 'Event Date', 'trim|required');
        $this->form_validation->set_rules('venue_id', 'Event Venue', 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
        }
        else
        {
            $this->Event_model->add();
            $this->session->set_flashdata('error', 'Record(s) added successfully.');
            $this->session->set_flashdata('error_code', 0);
        }

        redirect("/admin123/events");
    }

    public function edit($id)
    {
        $id = (int) $id;

        $data['venues'] = $this->Venue_model->getRowsDropdown();
        $data['event_time'] = $this->event_time();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Event Name', 'trim|required|max_length[2000]');
        $this->form_validation->set_rules('date', 'Event Date', 'trim|required');
        $this->form_validation->set_rules('venue_id', 'Event Venue', 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Update Event';  // set page title
            $data['row'] = $this->Event_model->getRows(0, 0, $id);
            if(empty($data['row']))
                redirect("/admin123/events");
            
            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'event-edit', $data);  // load content view
        }
        else
        {
            $this->Event_model->update($id);

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Record updated successfully!");

            redirect("/admin123/events");
        }
    }

    public function delete($id)
    {
        $data['row'] = $this->Event_model->delete($id);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Record deleted successfully!");
        redirect('/admin123/events', 'refresh');
    }

    private function event_time()
    {
        $start = 0;
        $end = 24;
        $period = '';
        $time = '';
        $output = array();

        for($start = $start; $start < $end; $start++ )
        {
            if($start < 12)
            {
                $time = $start;
                $period = 'AM';
            }
            else
            {
                $time = abs($start - 12);
                $period = 'PM';
            }

            if($time == 0) // change 0AM to 12AM
            {
                // echo "12 $period<br />";
                $output[] =  "12 $period";
            }
            else
            {
                // echo "$time $period<br />";
                $output[] =  "$time $period";
            }
        }
        return $output;
    }
}

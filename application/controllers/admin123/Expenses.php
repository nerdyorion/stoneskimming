<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expenses extends Admin_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            $this->load->model('Expense_model');
            $this->load->model('Event_model');
            $this->load->library("pagination");
    }

    public function index($event_id)
    {
        $data['event'] = $this->Event_model->getRows(0, 0, $event_id);
        if(empty($data['event']))
            redirect("/admin123/events");
        
        $user_id = (int) $this->session->userdata("user_id");

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        $header['page_title'] = $data['event']['name'] . ' ' . $data['event']['year'] . ' Expense(s)';

        // Pagination
        $config["base_url"] = base_url() . "admin123/expenses/index/$event_id";
        $config["total_rows"] = $this->Expense_model->record_count($event_id);
        $config["per_page"] = 10;
        $config["uri_segment"] = 5;
        $config['use_page_numbers']  = TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        $data["rows"] = $this->Expense_model->getRows($config["per_page"], $offset, $event_id);
        $data["links"] = $this->pagination->create_links();

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);
        $this->load->view($this->config->item('template_dir_admin') . 'menu');
        $this->load->view($this->config->item('template_dir_admin') . 'expenses', $data);
    }

    public function create($event_id)
    {
        $data['event'] = $this->Event_model->getRows(0, 0, $event_id);
        if(empty($data['event']))
            redirect("/admin123/expenses/index/$event_id");

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Expense Name', 'trim|required|max_length[2000]');
        $this->form_validation->set_rules('price', 'Price', 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
        }
        else
        {
            $this->Expense_model->add($event_id);
            $this->session->set_flashdata('error', 'Record(s) added successfully.');
            $this->session->set_flashdata('error_code', 0);
        }

        redirect("/admin123/expenses/index/$event_id");
    }

    public function edit($event_id, $id)
    {
        $data['event'] = $this->Event_model->getRows(0, 0, $event_id);
        if(empty($data['event']))
            redirect("/admin123/expenses/index/$event_id");

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Expense Name', 'trim|required|max_length[2000]');
        $this->form_validation->set_rules('price', 'Price', 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Update Expense';  // set page title
            $data['row'] = $this->Expense_model->getRows(0, 0, $event_id, $id);
            if(empty($data['row']))
                redirect("/admin123/expenses/index/$event_id");
            
            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'expense-edit', $data);  // load content view
        }
        else
        {
            $this->Expense_model->update($id);

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Record updated successfully!");

            redirect("/admin123/expenses/index/$event_id");
        }
    }

    public function delete($event_id, $id)
    {
        $data['row'] = $this->Expense_model->delete($event_id, $id);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Record deleted successfully!");
        redirect("/admin123/expenses/index/$event_id");
    }
}

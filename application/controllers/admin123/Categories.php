<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends Admin_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            $this->load->model('Category_model');
            $this->load->library("pagination");
    }

    public function index()
    {
        $user_id = (int) $this->session->userdata("user_id");

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        $header['page_title'] = 'Categories';

        // Pagination
        $config["base_url"] = base_url() . "admin123/categories/index";
        $config["total_rows"] = $this->Category_model->record_count();
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;
        $config['use_page_numbers']  = TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        $data["rows"] = $this->Category_model->getRows($config["per_page"], $offset);
        $data["links"] = $this->pagination->create_links();

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);
        $this->load->view($this->config->item('template_dir_admin') . 'menu');
        $this->load->view($this->config->item('template_dir_admin') . 'categories', $data);
    }

    public function create()
    {
        $user_id = (int) $this->session->userdata("user_id");

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Category Name', 'trim|required|max_length[2000]');
        $this->form_validation->set_rules('description', 'Category Description', 'trim|required|max_length[8000]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
        }
        else
        {
            $this->Category_model->add();
            $this->session->set_flashdata('error', 'Record(s) added successfully.');
            $this->session->set_flashdata('error_code', 0);
        }

        redirect("/admin123/categories");
    }

    public function edit($id)
    {
        $id = (int) $id;
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Category Name', 'trim|required|max_length[2000]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Update Category';  // set page title
            $data['row'] = $this->Category_model->getRows(0, 0, $id);
            if(empty($data['row']))
                redirect("/admin123/categories");
            
            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'category-edit', $data);  // load content view
        }
        else
        {
            $this->Category_model->update($id);

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Record updated successfully!");

            redirect("/admin123/categories");
        }
    }

    public function delete($id)
    {
        if((int) $id <= 7)
        {
            // system categories from 1 - 7 cannot be deleted as they are hardcoded and in use from the frontend
            $this->session->set_flashdata('error_code', 1);
            $this->session->set_flashdata('error', "Category cannot be deleted as it is used in the frontend logic");
            redirect('/admin123/categories');
        }
        $data['row'] = $this->Category_model->delete($id);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Record deleted successfully!");
        redirect('/admin123/categories', 'refresh');
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teams extends Admin_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            $this->load->model('Team_model');
            $this->load->model('Event_model');
            $this->load->library("pagination");

            /* Categories
             * 1 - Men
             * 2 - Women
             * 3 - Junior boys
             * 4 - Junior girls
             * 5 - Under-10 boys
             * 6 - Under-10 girls
             * 7 - Old Tossers (60 and over)
             *
             *
             * Breakdown
             * Adult Ladies and Men (16 and over) -------------- 5pound
             * Old Tossers Ladies and Men (60 and over) -------- 3pound
             * Junior Girls and Boys (10 - 15 years) ----------- 2pound
             * Under 10s Girls and Boys ------------------------ 1pound
             *
             * **Team members pay extra 1pound
             *
            */
    }

    public function index($event_id)
    {
        $data['event'] = $this->Event_model->getRows(0, 0, $event_id);
        if(empty($data['event']))
            redirect("/admin123/events");
        
        $user_id = (int) $this->session->userdata("user_id");
        // $data['former_teams'] = $this->Team_model->getRowsDropdown_TeamsBeforeYear($data['event']['year']);
        $data['former_events'] = $this->Event_model->getRowsDropdown_EventsBeforeYear($data['event']['year']);
        $data['teams_not_yet_full'] = $this->Team_model->getRowsDropDown_NotYetFull($event_id);

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        $header['page_title'] = $data['event']['name'] . ' ' . $data['event']['year'] . ' Team(s)';

        // Pagination
        $config["base_url"] = base_url() . "admin123/teams/index/$event_id";
        $config["total_rows"] = $this->Team_model->record_count($event_id);
        $config["per_page"] = 10;
        $config["uri_segment"] = 5;
        $config['use_page_numbers']  = TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        $data["rows"] = $this->Team_model->getRows($config["per_page"], $offset, $event_id);
        $data["links"] = $this->pagination->create_links();

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);
        $this->load->view($this->config->item('template_dir_admin') . 'menu');
        $this->load->view($this->config->item('template_dir_admin') . 'teams', $data);
    }

    public function getTeamsByEventID($event_id)
    {
        $event_id = (int) $event_id;

        $teams = $event_id == 0 ? NULL : $this->Team_model->getRowsDropDown($event_id);

        $output = '<option value="0" selected="selected">-- Select --</option>';

        if($teams)
        {
            foreach ($teams as $team) {
                $output .= '<option value="' . $team['id'] . '" >' . $team['name'] . '</option>';
            }
        }        

        echo $output;
    }

    public function create($event_id)
    {
        $data['event'] = $this->Event_model->getRows(0, 0, $event_id);
        if(empty($data['event']))
            redirect("/admin123/teams/index/$event_id");

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Team Name', 'trim|required|max_length[2000]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
        }
        elseif($this->Team_model->nameExists($event_id, trim($this->input->post('name'))))
        {
            $this->session->set_flashdata('error', 'Team name already exist for event.');
            $this->session->set_flashdata('error_code', 1);
        }
        else
        {
            $this->Team_model->add($event_id);
            $this->session->set_flashdata('error', 'Record(s) added successfully.');
            $this->session->set_flashdata('error_code', 0);
        }

        redirect("/admin123/teams/index/$event_id");
    }

    public function reusePastTeam($event_id)
    {
        $data['event'] = $this->Event_model->getRows(0, 0, $event_id);
        if(empty($data['event']))
            redirect("/admin123/teams/index/$event_id");

        $this->load->library('form_validation');
        $this->form_validation->set_rules('former_event_id', 'Past Event', 'trim|required');
        $this->form_validation->set_rules('former_team_id', 'Team', 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
        }
        elseif($this->Team_model->exists($event_id, (int) trim($this->input->post('former_team_id'))))
        {
            $this->session->set_flashdata('error', 'Team already exist for event.');
            $this->session->set_flashdata('error_code', 1);
        }
        else
        {
            $this->Team_model->reusePastTeam($event_id);
            $this->session->set_flashdata('error', 'Record(s) added successfully.');
            $this->session->set_flashdata('error_code', 0);
        }

        redirect("/admin123/teams/index/$event_id");
    }

    public function edit($event_id, $id)
    {
        $data['event'] = $this->Event_model->getRows(0, 0, $event_id);
        if(empty($data['event']))
            redirect("/admin123/teams/index/$event_id");

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Team Name', 'trim|required|max_length[2000]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Update Team';  // set page title
            $data['row'] = $this->Team_model->getRows(0, 0, $event_id, $id);
            if(empty($data['row']))
                redirect("/admin123/teams/index/$event_id");
            
            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'team-edit', $data);  // load content view
        }
        else
        {
            $this->Team_model->update($id);

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Record updated successfully!");

            redirect("/admin123/teams/index/$event_id");
        }
    }

    public function delete($event_id, $id)
    {
        $data['row'] = $this->Team_model->delete($event_id, $id);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Record deleted successfully!");
        redirect("/admin123/teams/index/$event_id", 'refresh');
    }

    public function view($event_id, $id)
    {
        $data['row'] = $this->Team_model->delete($event_id, $id);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Record deleted successfully!");
        redirect("/admin123/teams/index/$event_id", 'refresh');
    }

    private function event_time()
    {
        $start = 0;
        $end = 24;
        $period = '';
        $time = '';
        $output = array();

        for($start = $start; $start < $end; $start++ )
        {
            if($start < 12)
            {
                $time = $start;
                $period = 'AM';
            }
            else
            {
                $time = abs($start - 12);
                $period = 'PM';
            }

            if($time == 0) // change 0AM to 12AM
            {
                // echo "12 $period<br />";
                $output[] =  "12 $period";
            }
            else
            {
                // echo "$time $period<br />";
                $output[] =  "$time $period";
            }
        }
        return $output;
    }
}

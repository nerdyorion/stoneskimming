<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Winners extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();
        if(!is_logged_in())
        {
                //redirect to login
            redirect('/admin123/login');
        }
        $this->load->model('Participant_model');
        $this->load->model('Event_model');
        $this->load->library("pagination");

        /* Categories
             * 1 - Men                 (Male, 16 and over )
             * 2 - Women               (Female, 16 and over )
             * 3 - Junior boys         (Male, 10 - 15 )
             * 4 - Junior girls        (Female, 10 - 15 )
             * 5 - Under-10 boys       (Male,  < 10 )
             * 6 - Under-10 girls      (Female, < 10)
             * 7 - Old Tossers         (Male, Female, 60 and over)
             *
             *
             * Breakdown
             * Adult Ladies and Men (16 and over) -------------- 5pound
             * Old Tossers Ladies and Men (60 and over) -------- 3pound
             * Junior Girls and Boys (10 - 15 years) ----------- 2pound
             * Under 10s Girls and Boys ------------------------ 1pound
             *
             * **Team members pay extra 1pound
             *
        */
    }

    public function index($event_id)
    {
        $event_id = (int) $event_id;
        $data['event'] = $this->Event_model->getRows(0, 0, $event_id);
        if(empty($data['event']))
            redirect("/admin123/events");

        $user_id = (int) $this->session->userdata("user_id");

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        $header['page_title'] = $data['event']['name'] . ' ' . $data['event']['year'] . ' Winner(s)';

        // Pagination
        $config["base_url"] = base_url() . "admin123/winners/index/$event_id";
        // $config["total_rows"] = $this->Participant_model->record_count_winners($event_id);
        $config["total_rows"] = 3; // we just want first, second, third
        $config["per_page"] = 10;
        $config["uri_segment"] = 5;
        $config['use_page_numbers']  = TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        // $data["rows"] = $this->Participant_model->getRowsWinners($config["per_page"], $offset, $event_id);
        $data["rows"] = $this->Participant_model->getRowsWinners($event_id);
        $data["links"] = $this->pagination->create_links();

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);
        $this->load->view($this->config->item('template_dir_admin') . 'menu');
        $this->load->view($this->config->item('template_dir_admin') . 'winners', $data);
    }
}

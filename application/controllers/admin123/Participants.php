<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Participants extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();
        if(!is_logged_in())
        {
                //redirect to login
            redirect('/admin123/login');
        }
        $this->load->model('Participant_model');
        $this->load->model('Event_model');
        $this->load->model('Country_model');
        $this->load->model('Team_model');
        $this->load->model('Ticket_model');
        $this->load->model('Venue_model');
        $this->load->library("pagination");

        /* Categories
             * 1 - Men                 (Male, 16 and over )
             * 2 - Women               (Female, 16 and over )
             * 3 - Junior boys         (Male, 10 - 15 )
             * 4 - Junior girls        (Female, 10 - 15 )
             * 5 - Under-10 boys       (Male,  < 10 )
             * 6 - Under-10 girls      (Female, < 10)
             * 7 - Old Tossers         (Male, Female, 60 and over)
             *
             *
             * Breakdown
             * Adult Ladies and Men (16 and over) -------------- 5pound
             * Old Tossers Ladies and Men (60 and over) -------- 3pound
             * Junior Girls and Boys (10 - 15 years) ----------- 2pound
             * Under 10s Girls and Boys ------------------------ 1pound
             *
             * **Team members pay extra 1pound
             *
        */
    }

    public function index($event_id)
    {
        $event_id = (int) $event_id;
        $data['event'] = $this->Event_model->getRows(0, 0, $event_id);
        if(empty($data['event']))
            redirect("/admin123/events");

        $user_id = (int) $this->session->userdata("user_id");
        $data['countries'] = $this->Country_model->getRowsDropdown();
        $data['former_events'] = $this->Event_model->getRowsDropdown_EventsBeforeYear($data['event']['year']);
        $data['teams_not_yet_full'] = $this->Team_model->getRowsDropDown_NotYetFull($event_id);

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        $header['page_title'] = $data['event']['name'] . ' ' . $data['event']['year'] . ' Participant(s)';

        // Pagination
        $config["base_url"] = base_url() . "admin123/participants/index/$event_id";
        $config["total_rows"] = $this->Participant_model->record_count($event_id);
        $config["per_page"] = 10;
        $config["uri_segment"] = 5;
        $config['use_page_numbers']  = TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        $data["rows"] = $this->Participant_model->getRows($config["per_page"], $offset, $event_id);
        $data["links"] = $this->pagination->create_links();

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);
        $this->load->view($this->config->item('template_dir_admin') . 'menu');
        $this->load->view($this->config->item('template_dir_admin') . 'participants', $data);
    }

    public function getParticipantsByEventID($event_id)
    {
        $event_id = (int) $event_id;

        $participants = $event_id == 0 ? NULL : $this->Participant_model->getRowsDropDown($event_id);

        $output = '<option value="0" selected="selected">-- Select --</option>';

        if($participants)
        {
            foreach ($participants as $participant) {
                $output .= '<option value="' . $participant['id'] . '" >' . $participant['first_name'] . ' ' . $participant['last_name'] . '</option>';
            }
        }
        

        echo $output;
    }

    public function create($event_id)
    {
        $user_id = (int) $this->session->userdata("user_id");
        $data['event'] = $this->Event_model->getRows(0, 0, $event_id);
        if(empty($data['event']))
            redirect("/admin123/events");

        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('age', 'Age', 'trim|required');
        $this->form_validation->set_rules('gender', 'Gender', 'trim|required|max_length[50]');
        // $this->form_validation->set_rules('phone', 'Phone', 'trim|numeric|max_length[14]');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|max_length[50]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[255]|valid_email|is_unique[participants.email]');
        $this->form_validation->set_rules('address', 'Address', 'trim|required|max_length[8000]');

        $team_id = (int) trim($this->input->post('team_id'));

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
        }
        elseif(eventVenueFilled($event_id))
        {
            $this->session->set_flashdata('error', 'This event has reached its maximimum limit of participants. Kindly try again next year.');
            $this->session->set_flashdata('error_code', 1);
        }
        elseif($this->Team_model->is_filled($event_id, $team_id))
        {
            $this->session->set_flashdata('error', 'This team has reached its maximimum limit of participants.');
            $this->session->set_flashdata('error_code', 1);
        }
        else
        {
            // get category id by age and gender
            $category_id = getCategoryID(trim($this->input->post('age')), trim($this->input->post('gender')));
            $_POST['category_id'] = $category_id;

            $this->Participant_model->add($event_id);
            $this->session->set_flashdata('error', 'Record(s) added successfully.');
            $this->session->set_flashdata('error_code', 0);
        }

        redirect("/admin123/participants/index/$event_id");
    }

    public function reusePastParticipant($event_id)
    {
        $data['event'] = $this->Event_model->getRows(0, 0, $event_id);
        if(empty($data['event']))
            redirect("/admin123/participants/index/$event_id");

        $this->load->library('form_validation');
        $this->form_validation->set_rules('former_event_id', 'Past Event', 'trim|required');
        $this->form_validation->set_rules('former_participant_id', 'Participant', 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
        }
        elseif($this->Participant_model->exists($event_id, (int) trim($this->input->post('former_participant_id'))))
        {
            $this->session->set_flashdata('error', 'Participant already exist for event.');
            $this->session->set_flashdata('error_code', 1);
        }
        else
        {
            $this->Participant_model->reusePastParticipant($event_id);
            $this->session->set_flashdata('error', 'Record(s) added successfully.');
            $this->session->set_flashdata('error_code', 0);
        }

        redirect("/admin123/participants/index/$event_id");
    }

    public function edit($event_id, $id)
    {
        $id = (int) $id;
        $event_id = (int) $event_id;
        $data['event'] = $this->Event_model->getRows(0, 0, $event_id);
        if(empty($data['event']))
            redirect("/admin123/events");

        $data['countries'] = $this->Country_model->getRowsDropdown();
        $data['teams_not_yet_full'] = $this->Team_model->getRowsDropDown_NotYetFull($event_id);

        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('age', 'Age', 'trim|required');
        $this->form_validation->set_rules('gender', 'Gender', 'trim|required|max_length[50]');
        // $this->form_validation->set_rules('phone', 'Phone', 'trim|numeric|max_length[14]');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|max_length[50]');
        $this->form_validation->set_rules('address', 'Address', 'trim|required|max_length[8000]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Update Participant';  // set page title
            $data['row'] = $this->Participant_model->getRows(0, 0, $event_id, $id);
            if(empty($data['row']))
                redirect("/admin123/participants/index/$event_id");
            
            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'participant-edit', $data);  // load content view
        }
        else
        {
            // get category id by age and gender
            $category_id = getCategoryID(trim($this->input->post('age')), trim($this->input->post('gender')));
            $_POST['category_id'] = $category_id;

            $this->Participant_model->update($event_id, $id);

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Record updated successfully!");

            redirect("/admin123/participants/index/$event_id");
        }
    }

    public function delete($event_id, $id, $category_id, $paid)
    {
        $data['row'] = $this->Participant_model->delete($event_id, $id, $category_id, $paid);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Record deleted successfully!");
        redirect("/admin123/participants/index/$event_id", 'refresh');
    }

    public function removeFromTeam($event_id, $id)
    {
        $data['row'] = $this->Participant_model->removeFromTeam($event_id, $id);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Participant removed from team successfully!");
        redirect("/admin123/participants/index/$event_id", 'refresh');
    }

    public function markPaid($event_id, $id, $category_id, $team_id)
    {
        $team_id = (int) $team_id == 0 ? FALSE : $team_id;
        
        $price = getTicketPrice($event_id, $category_id, $team_id);
        $data['row'] = $this->Ticket_model->markPaid($event_id, $id, $category_id, $price);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Record deleted successfully!");
        redirect("/admin123/participants/index/$event_id", 'refresh');
    }

    public function markUnpaid($event_id, $id, $category_id, $team_id)
    {
        $data['row'] = $this->Ticket_model->markUnpaid($event_id, $id, $category_id);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Record deleted successfully!");
        redirect("/admin123/participants/index/$event_id", 'refresh');
    }

    private function event_time()
    {
        $start = 0;
        $end = 24;
        $period = '';
        $time = '';
        $output = array();

        for($start = $start; $start < $end; $start++ )
        {
            if($start < 12)
            {
                $time = $start;
                $period = 'AM';
            }
            else
            {
                $time = abs($start - 12);
                $period = 'PM';
            }

            if($time == 0) // change 0AM to 12AM
            {
                // echo "12 $period<br />";
                $output[] =  "12 $period";
            }
            else
            {
                // echo "$time $period<br />";
                $output[] =  "$time $period";
            }
        }
        return $output;
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Checkout extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        if(!is_logged_in())  // check if logged in
        {
            //redirect to login
            redirect('/login?return=checkout');
        }
        $this->load->model("Order_model");
        $this->load->model("Payment_Type_model");
        $this->load->model('Country_model');
    }

    public function index()
    {
        $header['page_title'] = 'Checkout';
        $header['page_desc'] = $header['page_title'] .' | Buy Chocolate Cakes and other variants.';
        $header['categories_all'] = $this->Category_model->getRowsDropDown();

        $header['payment_types'] = $this->Payment_Type_model->getRowsDropDown();
        $data['countries'] = $this->Country_model->getRowsDropDown();

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $data['rows'] = $this->getCart();

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('checkout', $data);  // load content view
    }

    public function secure()
    {
        $user_id = (int) $this->session->userdata("user_id");

        $this->load->library('form_validation');
        $this->form_validation->set_rules('payment_type_id', 'Payment Type', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('country_id', 'Delivery Country', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('address', 'Delivery Address', 'trim|required|max_length[8000]');

        if((is_null($this->input->post('payment_type_id'))) || ((int) $this->input->post('payment_type_id') == 0))
        {
            $errors = "Please select payment type!";
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', 1);
            redirect("/checkout");
        }
        elseif((is_null($this->input->post('country_id'))) || ((int) $this->input->post('country_id') == 0))
        {
            $errors = "Please select country!";
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', 1);
            redirect("/checkout");
        }
        elseif ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            redirect('checkout');
        }
        else
        {
            $payment_type_id = (int) $this->input->post('payment_type_id');
            $country_id = (int) $this->input->post('country_id');
            $country = $this->Country_model->getRows(0, 0, $country_id);
            $delivery_country_id = $this->input->post('country_id');
            $delivery_address = $this->input->post('address');

            if(strtoupper($country['name']) != 'QATAR')
            {
                $errors = 'Delivery to other other countries except <b>QATAR</b> is not allowed for now.';
                $this->session->set_flashdata('error', $errors);
                $this->session->set_flashdata('error_code', 1);

                redirect('checkout');
            }


            $cart = $this->getCart();
            $countItems = count($cart['cart_items']);
            $payload = '';

            $email_cart_details = '<ul>';

            // save order - get id
            $order_comment = NULL;
            $total_price = $cart['cart_total'];

            $order_id = $this->Order_model->add($total_price, $payment_type_id, $delivery_country_id, $delivery_address);

            // add order items
            foreach ($cart['cart_items'] as $key => $item) {
                // var_dump($item); die;
                $item_id = $item[0]['product_id'];
                $item_name = urldecode($item[0]['product_name']);
                $item_price = $item[0]['product_price'];
                $item_image = $item[0]['product_image'];
                $item_quantity = $item[0]['product_quantity'];
                $description .= "$item_name - $item_quantity, ";

                // generate cart details
                $email_cart_details .= "<b>$item_name</b> (&pound;" . number_format($item_price) . ")<br />";

                // add to db
                // $this->Order_model->addItems($order_id, $item_id, $item_name, $item_price);
                $this->Order_model->addDetails($order_id, $item_quantity, $item_id);
            }
            $email_cart_details .= "</ul>";
            $email_cart_details .= "<b>Total</b> &pound;" . number_format($cart['cart_total']) . "";

            $description = rtrim($description, ', ');

            // ---------------------add notification -----------------------------
            $student_name = $this->session->userdata('user_full_name');
            $to = $this->session->userdata('user_email');
            $subject = "New Order";

            $order_details .= '<b>Email</b>: ' . $to . '<br />';
            $order_details .= $email_cart_details . '<br /><br />';
            $order_details .= 'Regards,';

            $to = filter_var(strtolower($to), FILTER_SANITIZE_EMAIL);
            $to_name = filter_var(ucwords($student_name), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

            // $this->SEND_MAIL($subject, $message);

            // add notification to admin dashboard
            $notification_user_id = 0; // for super admin and admin

            $notification_message = 'New order from <a href="' . base_url() . 'admin123/users/view/' . $user_id . '">' . $to_name . '</a>';
            add_notification($notification_user_id, $notification_message);
            // --------------------------------------------------

            // clear cart
            $this->session->unset_userdata('cart');

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Payment successful. <a href='orders'>We will contact you on delivery shortly.&raquo;</a>");

            redirect('/Order-Complete');
        }
    }


    private function getCart()
    {
        // ------------- from fetch
        $cart_items = array();
        $cart_total = 0;

        $output = array();

        if(!is_null($this->session->userdata('cart')) && !empty($this->session->userdata('cart'))) // cart not empty
        {
            foreach ($this->session->userdata('cart') as $key => $item) {
                // var_dump($this->session->userdata('cart')); die;
                $cart = array(
                    'product_id' => $key,
                    'product_name' => $item['product_name'],
                    'product_name_short' => urldecode(ellipsize($item['product_name'], 33)),
                    'product_price' => number_format($item['product_price']),
                    'product_image' => urldecode($item['product_image']),
                    'product_quantity' => number_format($item['product_quantity'])
                );

                $cart_total += ($item['product_price'] * $item['product_quantity']);

                $cart_items[] = array($cart);
            }
            // add format
            $cart_total = number_format($cart_total); 
        }
        else // cart empty
        {
            redirect('/');
        }

        $output = array(
            'cart_items' => $cart_items,
            'cart_total' => $cart_total,
        );
        // ------------- from getch
        return $output;
    }











    public function fetch()
    {
        // echo "about to fetch :) ..."; die;
        $cart_items = array();
        $cart_total = 0;
        
        $output = array();

        if(!is_null($this->session->userdata('cart')) && !empty($this->session->userdata('cart'))) // cart not empty
        {
            foreach ($this->session->userdata('cart') as $key => $item) {
                // var_dump($this->session->userdata('cart')); die;
                $cart = array(
                    'product_id' => $key,
                    'product_name' => $item['product_name'],
                    'product_name_short' => urldecode(ellipsize($item['product_name'], 33)),
                    'product_price' => number_format($item['product_price']),
                    'product_image' => urldecode($item['product_image']),
                    'product_quantity' => number_format($item['product_quantity'])
                );

                $cart_total += ($item['product_price'] * $item['product_quantity']);

                $cart_items[] = array($cart);
            }
            // add format
            $cart_total = number_format($cart_total);
        }
        else // cart empty
        {
            // do nothing
        }

        $output = array(
            'cart_items' => $cart_items,
            'cart_total' => $cart_total,
        );

        echo json_encode($output);
    }
}
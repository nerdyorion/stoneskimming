<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            $this->load->model('Product_model');
            $this->load->library("pagination");
            // $this->load->helper('url_helper');
    }

    public function index($category_id)
    {
        $header['categories_all'] = $this->Category_model->getRowsDropDown();

        $data['category_name'] = $this->Category_model->exists($category_id);

        if(!$data['category_name'])
        {
            redirect('/');
        }

        $header['page_title'] = $data['category_name'];
        $header['page_desc'] = 'Purchase ' . $data['category_name'] . ' | Buy Chocolate Cakes and other variants.';

        $sort_price = isset($_GET['sort_price']) ? strtoupper(trim($_GET['sort_price'])) : FALSE;
        if($sort_price)
        {
            $config["total_rows"] = $this->Product_model->filter_record_count(FALSE, $category_id, FALSE, $sort_price);
        }
        else
        {
            $config["total_rows"] = $this->Product_model->record_count($category_id);
        }

        // Pagination
        // $config = array();
        $config["base_url"] = base_url() . "categories/index/$category_id";
        // $config["total_rows"] = $this->Product_model->record_count($category_id);
        $config["per_page"] = 3;
        $config["uri_segment"] = 4;
        $config['use_page_numbers']  = TRUE;
        $config['reuse_query_string']=TRUE;

        // Bootstrap 4 Pagination fix
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
        // $config['next_tag_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tag_close']  = '</span></li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tag_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tag_close']  = '</span></li>';
        /*
        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='active'><span><b>";
        // $config['cur_tag_close'] = "</b></span></li>";
        // $config['cur_tag_open'] = "<li class=\"active\"><a href=\"" . current_url() . "#\">";
        // $config['cur_tag_close'] = " <span class=\"sr-only\">(current)</span></a></li>";
        */

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
            // (page_no * per_page) - per_page
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        if($sort_price)
        {
            $data['rows'] = $this->Product_model->filter($config["per_page"], $offset, FALSE, $category_id, FALSE, $sort_price);
        }
        else
        {
            $data['rows'] = $this->Product_model->getRowsByCategoryID($config["per_page"], $offset, $category_id);
        }
        // $data['rows'] = $this->Product_model->getRowsByCategoryID($config["per_page"], $offset, $category_id);
        $data["links"] = $this->pagination->create_links();


        $this->load->view('header', $header);  // load header view
        $this->load->view('products', $data);  // load content view
    }

    public function view($id)
    {
        $data['row'] = $this->Product_model->getRows(0, 0, $id);

        if(empty($data['row']))
        {
            redirect("/");
        }


            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = $data['row']['name'];

            $data['rows'] = 'rows';
            $this->load->view('header', $header);  // load header view
            $this->load->view('product-details', $data);  // load content view
    }
}

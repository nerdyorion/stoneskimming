<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class MY_Controller extends CI_Controller
{
 
  function __construct()
  {
    parent::__construct();
    // $this->load->config('template_dir');
  }
}
 
class Admin_Controller extends MY_Controller
{
  function __construct()
  {
    parent::__construct();

    $this->load->config('template_dir');

    if((int) $this->session->userdata("user_role_id") != 1 && (int) $this->session->userdata("user_role_id") != 2)
    {
      redirect('/dashboard', 'refresh');
    }
  }
}
 
class Agent_Controller extends MY_Controller
{
  function __construct()
  {
    parent::__construct();

    $this->load->config('template_dir');

    if((int) $this->session->userdata("user_role_id") != 4)
    {
      redirect('/dashboard', 'refresh');
    }
  }
}
 
class Artist_Controller extends MY_Controller
{
  function __construct()
  {
    parent::__construct();

    $this->load->config('template_dir');

    if((int) $this->session->userdata("user_role_id") != 3)
    {
      redirect('/dashboard', 'refresh');
    }
  }
}
 
class Public_Controller extends MY_Controller
{
  function __construct()
  {
    parent::__construct();
  }
}
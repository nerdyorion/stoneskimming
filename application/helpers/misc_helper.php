<?php
defined('BASEPATH') OR exit('No direct script access allowed');


function getCategoryID($age, $gender)
{
  /* Categories
   * 1 - Men                 (Male, 16 and over )
   * 2 - Women               (Female, 16 and over )
   * 3 - Junior boys         (Male, 10 - 15 )
   * 4 - Junior girls        (Female, 10 - 15 )
   * 5 - Under-10 boys       (Male,  < 10 )
   * 6 - Under-10 girls      (Female, < 10)
   * 7 - Old Tossers         (Male, Female, 60 and over)
   *
  */

  $age = (int) $age;
  $gender = strtoupper($gender);
  $category_id = FALSE;

  // 16 - 59 MALE
  if(($age >= 16 && $age < 60) && $gender == 'MALE') // Men
  {
    $category_id = 1;
  }
  // 16 - 59 FEMALE
  elseif(($age >= 16 && $age < 60) && $gender == 'FEMALE') // Women
  {
    $category_id = 2;
  }
  // 10 - 15 MALE
  elseif(($age >= 10 && $age <= 15)  && $gender == 'MALE') // Junior boys
  {
    $category_id = 3;
  }
  // 10 - 15 FEMALE
  elseif(($age >= 10 && $age <= 15)  && $gender == 'FEMALE') // Junior girls
  {
    $category_id = 4;
  }
  // < 10 MALE
  elseif($age < 10 && $gender == 'MALE') // Under-10 boys
  {
    $category_id = 5;
  }
  // < 10 FEMALE
  elseif($age < 10 && $gender == 'FEMALE') // Under-10 girls
  {
    $category_id = 6;
  }
  // > 60 MALE and FEMALE
  elseif($age > 60 && ($gender == 'MALE' || $gender == 'FEMALE')) // Old Tossers
  {
    $category_id = 7;
  }
  else
  {
    // category not found
  }
  return $category_id;
}

function getTicketPrice($event_id, $category_id, $team_id = FALSE)
{
  /* Categories
   * 1 - Men                 (Male, 16 and over )
   * 2 - Women               (Female, 16 and over )
   * 3 - Junior boys         (Male, 10 - 15 )
   * 4 - Junior girls        (Female, 10 - 15 )
   * 5 - Under-10 boys       (Male,  < 10 )
   * 6 - Under-10 girls      (Female, < 10)
   * 7 - Old Tossers         (Male, Female, 60 and over)
   *
   *
   * Price
   * Adult Ladies and Men (16 and over) -------------- 5pound
   * Old Tossers Ladies and Men (60 and over) -------- 3pound
   * Junior Girls and Boys (10 - 15 years) ----------- 2pound
   * Under 10s Girls and Boys ------------------------ 1pound
   *
   * **Team members pay extra 1pound
   *
  */

  $CI =& get_instance();

  $category_id = (int) $category_id;
  $price = (float) $CI->Ticket_model->getPrice($event_id, $category_id);

  // if this is a team member, add 1pound to price
  if($team_id !== FALSE)
  {
    $price += 1;
  }

  return $price;
}

// check if venue is filled to determine whether to continue registration or buy ticket
function eventVenueFilled($event_id)
{
  $CI =& get_instance();
  return $CI->Venue_model->is_filled($event_id);
}

function base64url_encode($data) { 
  return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
} 

function base64url_decode($data) { 
  return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
}

function time_elapsed_string($datetime, $full = false) {
  $now = new DateTime;
  $ago = new DateTime($datetime);
  $diff = $now->diff($ago);

  $diff->w = floor($diff->d / 7);
  $diff->d -= $diff->w * 7;

  $string = array(
    'y' => 'year',
    'm' => 'month',
    'w' => 'week',
    'd' => 'day',
    'h' => 'hour',
    'i' => 'minute',
    's' => 'second',
  );
  foreach ($string as $k => &$v) {
    if ($diff->$k) {
      $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
    } else {
      unset($string[$k]);
    }
  }

  if (!$full) $string = array_slice($string, 0, 1);
  return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function notifications() {
  // Get current CodeIgniter instance
  $CI =& get_instance();
  $user_id = $CI->session->userdata('user_id');
  $user_role_id = $CI->session->userdata('user_role_id');
  $notifications = array();


  if($user_role_id == 1 || $user_role_id == 2) // Super Admin and Admin
  {
    // get notifications where user_id = 0
    $notifications = $CI->Notification_model->getRowsUnRead(0);

    return $notifications;
  }
  elseif($user_role_id == 3 || $user_role_id == 4) // Artist and Agent
  {
    // get notifications where user_id = $user_id
    $notifications = $CI->Notification_model->getRowsUnRead($user_id);

    return $notifications;
  }
  else
  {
    return false;
  }
}

function add_notification($user_id, $message) {
  // Get current CodeIgniter instance
  $CI =& get_instance();
  // $user_id = $CI->session->userdata('user_id');
  // $user_role_id = $CI->session->userdata('user_role_id');
  $user_id = (int) $user_id;
  // $message = filter_var($message, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
  
  $CI->Notification_model->add($user_id, $message);
  return true;
}

function dashIfEmpty($var, $default = '-') {
  return empty($var) ? $default : $var;
}

function image_thumb($image_url)
{
  if(!empty($image_url))
  {
    $image_url_thumb = "";

    $arr = explode(".", $image_url);
    $ext = array_pop($arr);
    $image_url_thumb = implode('.', $arr) . '_thumb.' . $ext;

    return $image_url_thumb;
  }
  else
  {
    return $image_url;
  }
}


?>